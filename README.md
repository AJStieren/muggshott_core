# muggshott_core

This is a core repository for various projects, documentation, and infrastructure by code setups. Here I will create various setup files and resources for establishing a normal home environment, document various projects and their statuses (potentially plans), and address the infrastructure behind these elements. Effectively this is an unprofessional brain dump repository guaranteeing version control and availability.

## Tools

- [ ] Docker
- [ ] Puppet
- [x] Itermocil (Mac OS Only)
- [ ] Ansible

## Projects

- [ ] DnD
  - [ ] Out of the Abyss
  - [ ] Gethenhol
- [ ] Homelab
  - [x] MineOS
  - [x] pfSense
  - [ ] Ubuntu RDS
  - [x] Manjuro -RDS- Desktop
  - [x] Theia IDE
  - [x] Container Environment
- [x] NAS Server
  - [x] Note Station
  - [x] Photo Backup
  - [x] Media Server
  - [ ] Automatic Backups
  - [x] RAID 0
- [ ] Minecraft
  - [ ] Compact
  - [x] GT6
  - [ ] MiyCraft
  - [x] Omnifactory
  - [ ] Stoneblock 2
- [x] Pokemon
- [ ] Profession
  - [ ] Brainstorm
  - [ ] Features
  - [x] Goals
  - [x] Scrum
