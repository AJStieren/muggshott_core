#!/bin/bash

mkdir -p ~/EoI/Projects/{Cloud_Feeds,Repose,Tesla,Volatile_Workspace}
mkdir -p ~/EoI/{Access,Library}
mkdir -p ~/EoI/Tools/{Theia,Utility_Scripts,Maven}
mkdir -p ~/Workspace/{CFLogs,Current,Debug,Mess,Volatile_Workspace}
