# Server 715

## Heroes on Deck

These are the heroes in active rotation and the level of ascension they've achieved.

| Hero     | Faction     | Ascension      | SI  | Furniture |
| -------- | ----------- | -------------- | --- | --------- |
| Gwyneth  | Lightbearer | Ascended +1    | 25  | 3/9       |
| Rowan    | Lightbearer | Ascended +1    | 30  | 1/9       |
| Cecilia  | Lightbearer | Ascended +1    | 10  | 3/9       |
| Estrilda | Lightbearer | Ascended +1    | 0   | 0/9       |
| Hendrik  | Lightbearer | Ascended       | 0   | 0/9       |
| Numisu   | Mauler      | Ascended       | 15  | 3/9       |
| Kren     | Mauler      | Ascended       | 20  | 3/9       |
| Saurus   | Wilder      | Ascended +2    | 30  | 1/9       |
| Tasi     | Wilder      | Ascended       | 0   | 3/9       |
| Raku     | Wilder      | Ascended       | 0   | 0/9       |
| Ferael   | Graveborn   | Ascended       | 20  | 1/9       |
| Thoran   | Graveborn   | Ascended       | 20  | 0/9       |
| Talene   | Celestial   | Legendary      | N   | N/9       |
| Mortus   | Hypogean    | Legendary+     | N   | N/9       |
| Queen    | Dimensional | Linked Saurus  | 0   | 2/9       |
| Ainz     | Dimensional | Linked Gwyneth | 23  | 2/9       |
| Albedo   | Dimensional | Linked Cecilia | 10  | 3/9       |
| Arthur   | Dimensional | Linked Thoran  | 20  | 1/9       |
| Joker    | Dimensional | Linked Numisu  | 0   | 3/9       |

## In the Barrel

These are the heroes in each faction being worked on. Celestials and Hypogeans are combined for the purposes of Stargazing and will not include currency characters such as Ezizh and Wu Kong.

| Lightbearer | Mauler | Wilder | Graveborn        | Celepogean         |
| ----------- | ------ | ------ | ---------------- | ------------------ |
| ????        | Warek  | Lyca   | Daimon or Desira | Mortus -> Lucretia |

## Backlog

These are the heroes ready to be built waiting on fodder

| Hero     | Faction     | Copies | Ascension  |
| -------- | ----------- | ------ | ---------- |
| Fawkes   | Lightbearer | 10     | Elite+     |
| Raine    | Lightbearer | 7      | Elite+     |
| Rosaline | Lightbearer | 5      | Elite+     |
| Belinda  | Lightbearer | 7      | Elite+     |
| Brutus   | Mauler      | 4      | Mythic     |
| Warek    | Mauler      | 8      | Legendary+ |
| Skriath  | Mauler      | 8      | Elite+     |
| Vurk     | Mauler      | 7      | Elite+     |
| Lyca     | Wilder      | 7      | Elite+     |
| Eironn   | Wilder      | 8      | Elite+     |
| Pippa    | Wilder      | 5      | Elite+     |
| Respen   | Wilder      | 9      | Elite+     |
| Nara     | Graveborn   | 6      | Mythic     |
| Daimon   | Graveborn   | 9      | Legendary+ |
| Grezhul  | Graveborn   | 4      | Elite+     |
| Twins    | Celestial   | 2      | Elite+     |
| Talene   | Celestial   | 4      | Legendary  |
| Alna     | Celestial   | 2      | Elite+     |
| Mortus   | Hypogean    | 7      | Legendary+ |
| Lucretia | Hypogean    | 2      | Elite+     |
| Ezizh    | Hypogean    | 2      | Elite+     |