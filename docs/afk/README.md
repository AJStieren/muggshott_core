# AFK Arena

AFK Arena is a mobile game published by Lilith, a Chinese gaming company. While there is a significant amount of pay to win (P2W) elements in the game, the lack of PVP and generous passive rewards allow for a gratifying free to play (F2P) experience.

Lilith regularly hosts a number of events and grants portions of its "premium" currency in regular intervals as compensation for downtime activies such as patching or hotfixes. In addition, Lilith uses a code distribution method in order to grant free resources to players over time. These codes can occur in a number of social media
