# Resource Values
| Reward              | Diamond Value |
| ------------------- | ------------- |
| 1h Gold             | 2             |
| 1h Dust             | 8             |
| 1h Exp              | 8             |
| Guild Coins         | .04 Diamonds  |
| 400 Twisted Essence | 5             |
| Purple Stones       | 36            |
| Blue Stones         | 4             |
| Scroll              | 270           |
| Purple Emblems      | 24            |
| Gold Emblems        | 62.4          |
| Red Emblems         | 158.4         |
| Poe Coins           | .625          |
| Hero Choice         | 6400          |
| T1 Stone            | 1473          |
| T2 Stone            | 1635          |

## Dreamy Chest

| Number | Reward          | Diamond Value |
| ------ | --------------- | ------------- |
| 3      | Scrolls         | 810           |
| 12     | Gold Emblems    | 748.8         |
| 3      | 24h Dust        | 576           |
| 3      | 24h Exp         | 576           |
| 900    | Poe Coins       | 562.5         |
| 20     | Purple Emblems  | 480           |
| 90     | Twisted Essence | 450           |

## Chest of Wishes

| Number | Reward          | Diamond Value |
| ------ | --------------- | ------------- |
| 8      | Gold Emblems    | 499.2         |
| 3      | Red Emblems     | 475.2         |
| 600    | PoE Coins       | 375           |
| 13     | Purple Emblems  | 312           |
| 60     | Twisted Essence | 300           |
| 1      | Faction Scroll  | 270           |

## Chest of Wishes

| Number | Reward          | Diamond Value |
| ------ | --------------- | ------------- |
| 12     | Gold Emblems    | 748.8         |
| 9      | 8h Dust         | 576           |
| 2      | Scrolls         | 540           |
| 20     | Purple Emblems  | 480           |
| 80     | Twisted Essence | 400           |
