# Joker Bountiful (Expired)

Top to bottom, left to right

## Fight 1

Flora - Rowan<br>
Joker - Eironn - Fawkes

## Fight 2

Joker - Flora<br>
Rowan - Eironn - Fawkes

## Fight 3

Hendrik - Thoran<br>
Ezio - Joker - Anoki

## Fight 4

Skregg - Zaphrael<br>
Joker - Rowan - Rosaline

## Fight 5

Hendrik - Ezio<br>
Nara - Fawkes - Joker

## Fight 6

Arthur - Izold<br>
Joker - Rowan - Gwyneth
