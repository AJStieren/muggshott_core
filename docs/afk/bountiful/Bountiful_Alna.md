# Alna Bountiful (Expired)

Two columns of fights. Teams 1-6 top to bottom and 7-12 top to bottom.

## Team 1

Alna - Zaphrael<br>
Ezizh - Oscar - Skriath

## Team 2

Alna - Antandra<br>
Wu Kong - Daimon - Zolrath

## Team 3

Alna - Zolrath<br>
Gorvo - Eironn - Daimon

## Team 4

Alna - Antandra<br>
Wu Kong - Daimon - Zolrath

## Team 5

Daimon - Mezoth<br>
Athalia - Lyca - Alna

## Team 6

Daimon - Mezoth<br>
Alna - Lyca - Athalia

## Team 7

Alna - Zaphrael<br>
Ezizh - Skriath - Oscar

## Team 8

Alna - Ezizh<br>
Oscar - Skriath - Zaphrael

## Team 9

Alna - Thorem<br>
Zaphrael - Skriath - Tidus

## Team 10

Arthur - Alna<br>
Wu Kong - Antandra - Rosaline

## Team 11

Mezoth - Alna<br>
Athalia - Rowan - Lyca

## Team 12

Mezoth - Alna<br>
Rowan - Athalia - Lyca
