# Queen Bountiful (Expired)

Top to bottom, left to right

## Fight 1

Queen - Anoki<br>
Vurk - Nara - Skriath

## Fight 2

Queen - Anoki<br>
Vurk - Nara - Skriath

## Fight 3

Queen - Orthros<br>
Rowan - Skriath - Belinda

## Fight 4

Queen - Aphelios<br>
Belinda - Rowan - Arthur

## Fight 5

Queen - Orthros<br>
Belinda - Rowan - Skriath

## Fight 6

Queen - Aphelios<br>
Belinda - Rowan - Arthur
