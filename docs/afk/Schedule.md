# Regular Events

## Daily

* Loot x2
* Friends
* Shop
* Battle
* Arena
* Guild Boss
* Fast Forward
* Tower

### Faction Tower

| --          | Sunday | Monday | Tuesday | Wednesday | Thursday | Friday | Saturday |
| ----------- | ------ | ------ | ------- | --------- | -------- | ------ | -------- |
| Lightbearer | [x]    | [x]    | [ ]     | [ ]       | [ ]      | [x]    | [ ]      |
| Mauler      | [x]    | [ ]    | [x]     | [ ]       | [ ]      | [x]    | [ ]      |
| Wilder      | [x]    | [ ]    | [ ]     | [x]       | [ ]      | [ ]    | [x]      |
| Graveborn   | [x]    | [ ]    | [ ]     | [ ]       | [x]      | [ ]    | [x]      |

### QOD

* Labrynth

## Events

There are regular events which occur.
