# Ages of Greg

A category of `age` mod in which some content is locked behind defined ages. Rather than having explicit functionality, this component provides an interface unique to `iig` modules for age definitions.

## Age by Achievment

This follows the stereotypical route of quest modpacks in progressing in age based on achieving a goal defined in a quest in some way. Prime examples include defining an age to start at the point of entering the Nether, creating a specific multiblock, or crafting certain materials for the first time.
