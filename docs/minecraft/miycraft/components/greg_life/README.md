# Template

Hostile mobs, passive creatures, and loot drop changes are here. These are entities which are can move and/or interact with the environment. Any potential tamed pets or adjustment to existing hostile mobs will be here.

## Hostile Mobs

These are additional hostile mobs which harass the player throughout the game.

### Instance

An instance of the category which may be a specific implementation or an abstraction which is altered based on material.

#### Integration

Details regarding the integration with other components if present.
