# Greg World

A defined world generation which affects stone types, ore gen, and non-functional plantlife. Procedural structure generation, adaptation of dungeons, and potentially villager buildings are also within the scope of this component. This will also include definitions for the basic metals correlating to the components in question

## Stone Types

### Red Clay

### Red Granite

### Black Granite

### Marble

### Schist

### Gneiss

### Slate ???

### Quartzite

### Basalt

### Limestone

### Migmatite

### Shale

### Hornfels

### Mylonite

### Komatiite

### Kimberlite

### Rock Salt

### Shale (Oil Integration)

## Ores

### Brown Limonite

### Yellow Limonite

### Banded Iron

### Iron

### Pyrite

### Hematite

### Magnetite

### Vanadium Magnetite

### Tetrahedrite

### Copper

### Chalcopyrite

### Stibnite

### Tin

### Casseterite

### Galena

### Silver

### Lead

### Gold

### Electrum ???

### Platinum

### Iridium

### Palladium

### Osmium

### Cobaltite

### Garnerite

### Nickel

### Pentlandite

### Uranium 238

### Uranite

### Thorium

### Coal

### Lignite Coal

### Scheelite

### Tungstate

### Ferberite

### Wolframite

### Russellite

### Powllite

### Monazite

### Bastnasite

### Lithium

## Structures

### Precursor Facility

The Precursor Facility structure is a rarely occuring structre in the world which is generated on a chunk by chunk basis. Similar to GT6 dungeons in concept, the precursor facility is procedurally generated with functional rooms. Basic treasure rooms are a given, but integration with components adds rooms based on the lowest possible functional age available with few exceptions. No ZPMs. Seriously, fuck that.

#### Material Structure

The materials used for the structure are based off of available stones which extend with Greg World. Materials in construction can be categorized as primary, secondary, and accent color. Walls are constructure out of a brick form of the primary material, panels near the floor, and a gap with foam of the accent color. The secondary material tiles the walkways flanked by panels of the primary material.

Glowstone Lamps are used for lighting overhead with 6x6 hallways connecting rooms. Doors at transition are sticky piston are symmetrical redstone buttons.

#### Rooms

##### Hallway

Hallways are 6x6 straight shots across chunks with the potential to come to a tee or a cross.

##### Treasure Room

Treasure rooms contain a significant amount of materials and loot.

_With Stored in Greg_
Mass Storage boxes filled with primary and secondary pieces of the Precursor Facility

Third lowest chest tier of loot

##### Boiler Room

_Requires Steam Age_
Contains a large boiler, some pipes, and some steam consumption devices.

## Villager Buildings

## Dungeon Adaptation

???
