# Agriculture

## Food Crops

### Rye

### Oats

### Rice

### Corn

### Seaweed ???

### Blueberry

### Strawberry

### Raspberry

### Cranberry

### Gooseberry

### Banana

### Tomato

### Onion

### Cucumber

### Okra

### Broccoli

### Cauliflower

### Spaghetti Squash

### Butternut Squash

### Summer Squash

### Zucchini

### Garlic

### Bell Pepper

### Jalapeno

### Poblano

### Almond

### Peanut

### Bean

### Sweet Potato

### Red Grape

### White Grape

### Green Grape

### Purple Grape

### Black Currant

### White Currant

### Red Currant

### Pomegranate

### Lemon

### Lime

### Orange

### Ananas

### Mint

### Vanilla

### Cinnamon

### Nutmeg

### Coriander

### Spice Leaf

This is totally cribbed from Harvestcraft. The spice leaf is itself a standin for certain kinds of spices and can be dried out as a root for root beer.

### Celery

### Scallion

### Lettuce

### Cabbage

### Eggplant

### Avocado

### Peach

### Plum

### Kiwi

### Starfruit

### Dragonfruit

### Pear

### Coconut

### Pear

### Beat

### Rubarb

### Candleberry

### Red Apple

### Green Apple

### Dark Red Apple

### Yellow Apple

### Hazlenut

### Pistachio Nut

### Apricot

## Dye Flowers

### Tulip

## Functional Crops

### Flax

### Canola

### Aloe Vera

### 

## Floral Indicators

### Orechid

| Tungstate |
| Ferberite |
| Wolframite |
| Stolzite |
| Scheelite |
| Huebnerite |
| Russeltite |
| Pinalite |
| Monazite |
| Powellite |
| Vanadium |
| Graphite |
| Hematite |
| Bauxite |
| Cassiterite |
| Chalcopyrite |
| Naquadah |

### Tufted Eventing Primrose

| Uranium |
| Uraninite |

### Narcissus Sheldonia

| Cooperite ??? |
| Platinum |

### Desert Trumpet

| Gold |
| Electrum |

### Copper Flower Plant

| Copper |
| Chalcopyrite |

### Crosby Buckwheat

| Galena |
| Silver |
| Lead |