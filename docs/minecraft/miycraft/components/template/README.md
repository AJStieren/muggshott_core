# Template

This is a template for component README markdown documents. A description of the module, brief or verbose, goes here to convey the intent of the component.

## Category

This is a category of content within the mod. This will usually refer to a concept rather than a specific implementation such that a generalization can be made for content under this header.

### Instance

An instance of the category which may be a specific implementation or an abstraction which is altered based on material.

#### Integration

Details regarding the integration with other components if present.
