# muggshott_core MiyCraft

Miycraft (Make It Your Craft) is a concept Minecraft mod suite conceived as a bridge between the Immersive Engineering aesthetic and the complexity derived from a Gregtech styled mod. It's difficult to not directly copy off of either mod when pushing torward the goal, but as a suite it would be nice to achieve some amount of separation. In addition, several other concept are intended to be integrated as component mods to IIG for agricultural gameplay, pethrochemical processing, and age based progression.

## IIG Suite Mods

### Miy World

A defined world generation which affects stone types, ore gen, and non-functional plantlife. Procedural structure generation, adaptation of dungeons, and potentiall villager buildings are also within the scope of this component. This will also include definitions for the basic metals correlating to the components in questiony

### Miy Life

Hostile mobs, passive creatures, and loot drop changes are here. These are entities which are can move and/or interact with the environment. Any potential tamed pets or adjustment to existing hostile mobs will be here.

### Ages of Miy

A category of `age` mod in which some content is locked behind defined ages. Rather than having explicit functionality, this component provides an interface unique to `iig` modules for age definitions.

### MetaMiy

Meta tools and their functionality separated out for dependencies. These are tools which will follow the GT6 or GTCE model of having a base entity object which populates its stats based on the materials used in their construction. Terms of balancing may or may not be confined to the mod sourcing the material leaving only vanilla material metatool concrete definitions here.

### Stored in Miy

Storage solutions such as barrels, mass storage, chests, etc.. This will **not** include item mobility or innate filtering. This will keep this component very light and easily dropped or included anywhere.

Open question: Will this include fluid storage?

### Agriculture

Plantlife and crops along with associated processing when paired with other mods.

### Architecture

Materials applied to architecture blocks which non-functional split to this module in order to decrease bloat when other building mods are included. This will generate in a manner similar to meta-tools in that the materials available will be dynamically assessed. Core objects includes but are not limited to the following:

<details>
  <summary></summary>

* Stairs
* Slabs
* Wedges
* Buttons
* Fence(s)

</details>

### Mechanics

Mechanical and/or manual power constructs for processing. This constitutes the stone and iron age without entanglement and will extend through other ages such as the bronze age when combined with other components.

### Steam Age

Starting in the bronze age, steam can be used to perform processing functional. The definitions of these functions will stem from the core of IIG tech, but will also encompass many additional functions pulled from added components as desired. Configuration will be implicit based on component inclusion rather than having explicit enablement definitions.

### Steam Punk

Extending past pure mechanical processing, the steam punk component enters more fanciful but advanced functionality still within the context of a steam based power structure. Flight, steam-based tools, and potentially steampunk power armor operating on a pressure system.

### Electrical Age

Electrical processing functionality and machinery. This will also extend to basic tool functionality in the form of drills, wrenches, chainsaws, etc. Motors and pistons will be tier based both in terms of voltage as well as the tensile strength of the materials involved. This will also extend the materials involved in production beyond the extend of steam metals and such.

### Petrochem

Oil. Liquid Fuel. Natural Gas. Plastics. Etc..

The chemical processing will be tiered such that multiple stopping points will be available for materials. Explosives are likely to be an extension of this module. Open question on terms of generation.

### Biochem

Biochemical processing with the renewable fueling target of ethanol or biodiesel.

### Atomic Age

Nuclear material and power. Radiation possibilities as well.
