############################
# Device Registration 
############################

# Devices are block or multiblock structures which transform inputs into outputs.
# Devices are referenced in Processes where inputs and outputs are localized.

# Device specification
# device: Name of the device
# mod: Mod of device origin
# options: Optional list of upgrades or configuration items which alter device behavior

deviceRegistry:
  - &sampleObject
    name: -change me-
    mod: -change me-
  - &cobblestoneGenerator
    name: Cobblestone Generator
    mod: Tiny Progressions
  - &lootFabricator
    name: Loot Fabricator
    mod: Depp Mob Learning
  - &simulationChamber
    name: Simulation Chamber
    mod: Deep Mob Learning
    options:
      - Blaze
      - Creeper
      - Elemental
      - Ender Dragon
      - Enderman
      - Ghast
      - Guardian
      - Shulker
      - Skeleton
      - Slime
      - Spider
      - Witch
      - Wither
      - Wither Skeleton
      - Zombie
  - &furnace
    name: Furnace
    mod: Minecraft
  - &electricFurnace
    name: Electric Furnace
    label: Furnace
    mod: Gregtech
  - &alloySmelter
    name: Alloy Smelter
    mod: Gregtech
  - &compressor
    name: Compressor
    mod: Gregtech
  - &lathe
    name: Lathe
    mod: Gregtech
  - &wiremill
    name: Wiremill
    mod: Gregtech
  - &extruder
    name: Extruder
    mod: Gregtech
    options:
      - Gear Mold
      - Small Gear Mold
      - Wire Mold
      - Bolt Mold
      - Rod Mold
      - Long Rod Mold
      - Sheet Mold
      - Tiny Pipe Mold
      - Small Pipe Mold
      - Medium Pipe Mold
      - Large Pipe Mold
      - Huge Pipe Mold
      - Block Mold
  - &mixer
    name: Mixer
    mod: Gregtech
  - &electrolyzer
    name: Electrolyzer
    mod: Gregtech
  - &centrifuge
    name: Centrifuge
    mod: Gregtech
  - &macerator
    name: Macerator
    mod: Gregtech
  - &oreWasher
    name: Ore Washer
    mod: Gregtech
  - &thermalCentrifuge
    name: Thermal Centrifuge
    mod: Gregtech
  - &electricBlastFurnace
    name: Electric Blast Furnace
    mod: Gregtech
    label: multiblock
  - &vacuumFreezer
    name: Vacuum Freezer
    mod: Gregtech
    label: multiblock
  - &assembler
    name: Assembly Machine
    mod: Gregtech
    option:
      - Configuration Circuit
  - &distillery
    name: Distillery
    mod: Gregtech
    options:
      - Configuration Circuit
  - &brewery
    name: Brewery
    mod: Gregtech
    options:
      - Configuration Circuit
  - &fermenter
    name: Fermenter
    mod: Gregtech
    options:
      - Configuration Circuit
  - &chemicalReactor
    name: Chemical Reactor
    mod: Gregtech
    options:
      - Configuration Circuit
  - &distillationTower
    name: Distillation Tower
    mod: Gregtech
    label: multiblock
  - &pyrolyseOven
    name: Pyrolyse Oven
    mod: Gregtech

############################
# Process Registration 
############################

# Processes are transformations of items into specified output. Either the input or 
# output of a process must be unified such that one of the following is true:
# 1. All inputs create the same output
# 2. The output is created from all inputs
# 3. All outputs are created from all inputs

# Process Specification
# process: Name of process
# device: The device used in the process
#    - Dereference from device registry with * operator
# input: List of items or resources consumed during the process
# output: List of items or resources newly produced during the process
# option: Options which modify the output of a device should be specified

processRegistry:
  - &fabricateBlazeRod
    process: Fabricate Blaze
    device: *lootFabricator
    input:
      - Blaze Essence
    output:
      - Blaze Rod
  - &fabricateSulfur
    process: Fabricate Sulfur
    device: *lootFabricator
    input:
      - Blaze Essence
    output:
      - Sulfur
  - &fabricateMagma
    process: Fabricate Magma
    device: *lootFabricator
    input:
      - Blaze Essence
    output:
      - Magma Block
  - &fabricateCoal
    process: Fabricate Coal
    device: *lootFabricator
    input:
      - Creeper Essence
    output:
      - Coal
  - &fabricateGunpower
    process: Fabricate Gundpower
    device: *lootFabricator
    input:
      - Creeper Essence
    output:
      - Gunpowder
  - &fabricate
    process: Fabricate
    device: *lootFabricator
    input:
      - Essence
    output:
  - &fabricateGold
    process: Fabricate Gold
    device: *lootFabricator
    input:
      - Guardian Essence
    output:
      - Gold Ingot
  - &fabricateAluminium
    process: Fabricate Aluminium
    device: *lootFabricator
    input:
      - Guardian Essence
    output:
      - Aluminium Dust
  - &fabricateIron
    process: Fabricate
    device: *lootFabricator
    input:
      - Zombie Essence
    output:
      - Iron Essence
