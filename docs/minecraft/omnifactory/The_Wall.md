# The Wall

The wall of storage use to describe the output of passively generated resources. The registry method for obtaining the materials will not be included.

## Tier One

Tier One materials are first order made either through cobbleworks or direct DML drops without crafting or alloying.

### Cobbleworks

* Cobblestone
* Stone
* Gravel
* Sand
* Clay
* Aluminium
* Silicon
* Lithium
* Sodium
* Sandstone
* Saltpeter
* Glass
* Nether Quartz

### DML

| Data Module       | Tier             | Product               | Power Consumption |
| ----------------- | ---------------- | --------------------- | ----------------- |
| Zombie            | Overworldian     | Iron Ingot            | 64 RF/t           |
| Skeleton          | Overworldian     | Tin Ingot             | 64 RF/t           |
| Spider            | Overworldian     | Copper Ingot          | 64 RF/t           |
| Creeper           | Overworldian     | Coal                  | 64 RF/t           |
| Slime             | Overworldian     | Nickel Coin           | 64 RF/t           |
| Slime             | Overworldian     | Platinum Coin         | 64 RF/t           |
| Witch             | Overworldian     | Redstone              | 512 RF/t          |
| Witch             | Overworldian     | Glowstone             | 512 RF/t          |
| Guardian          | Overworldian     | Gold Ingot            | 512 RF/t          |
| Guardian          | Overworldian     | Aluminium Dust        | 512 RF/t          |
| Thermal Elemental | Overworldian     | Blizz                 | 1024 RF/t         |
| Thermal Elemental | Overworldian     | Blitz                 | 1024 RF/t         |
| Thermal Elemental | Overworldian     | Basalz                | 1024 RF/t         |
| Thermal Elemental | Overworldian     | Obsidian Dust         | 1024 RF/t         |
| Thermal Elemental | Overworldian     | Saltpeter             | 1024 RF/t         |
| Blaze             | Hellish          | Blaze Rod             | 1024 RF/t         |
| Ghast             | Hellish          | Silver Ingot          | 1024 RF/t         |
| Wither Skeleton   | Hellish          | Lead Ingot            | 1024 RF/t         |
| Wither            | Hellish          | Wither Realm Data     | 6666 RF/t         |
| Shulker           | Extraterrestrial | Diamond               | 512 RF/t          |
| Enderman          | Extraterrestrial | Enderpearl            | 2048 RF/t         |
| Enderman          | Extraterrestrial | Emerald               | 2048 RF/t         |
| Enderman          | Extraterrestrial | Impossible Realm Data | 2048 RF/t         |
| Ender Dragon      | Extraterrestrial | Dragon's Breath       | 6666 RF/t         |
| Ender Dragon      | Extraterrestrial | Ender Dragon Scale    | 6666 RF/t         |


* Iron
* Copper
* Tin
* Gold
* Silver
* Lead
* Nickel
* Platinum
* Coal
* Blaze Rods
* Ender Pearl
* Emerald
* Diamond
* Redstone
* Glowstone

## Tier Two

### DML

#### Blaze Processing

* Sulfur
* Carbon
* Magnesium
* Calcium
* Oxygen
* Potassium
* Sodium
* Phosphoric Acid

#### Coal

* Void Crystal
* Red Coal

#### Alloys & Other Metals

* Wrought Iron
* Bronze
* Tin Alloy
* Red Alloy
* Steel
* Dark Steel
* Electric Steel
* Electrum
* Invar ??
* Cupronickel ??
* Conductive Iron
* Energetic Alloy
* Vibrant Alloy
* Annealed Copper

### Other

* Pulsating Dust
* Pulsating Iron
* Pulsating Polymer Clay
* Conduit Binder
* Sodium Hydroxide
* Hydrogen
* Sulfuric Acid

### Tier 3

* Black Steel
* Blue Steel