# muggshott_core: Omnifactory

Currently way too into a minecraft modpack called Omnifactory. Omnifactory is largely a Gregtech Community Edition pack with a very linear progression (relative to GT6)

## TODO

### Tiers

<details>
  <summary>GT Voltage</summary>

#### GT Machine Tiers

- [x] LV
- [x] MV
- [x] HV
- [x] EV
- [ ] IV
- [ ] LuV
- [ ] ZPM
- [ ] MAX

</details>

<details>
  <summary>Circuits</summary>

#### Circuit Tiers

- [x] Tier 1 - Refined
  - [x] Circuit
  - [x] Processor
  - [x] Processor Array
  - [x] Framework
- [x] Tier 2 - Micro
  - [x] Circuit
  - [x] Processor
  - [x] Processor Array
  - [x] Framework
- [x] Tier 3 - Nano
  - [x] Circuit
  - [x] Processor
  - [ ] Processor Array
  - [ ] Framework
- [ ] Tier 4 - Quantum
  - [ ] Circuit
  - [ ] Processor
  - [ ] Processor Array
  - [ ] Framework
- [ ] Tier 5 - Crystal
  - [ ] Circuit
  - [ ] Processor
  - [ ] Processor Array
  - [ ] Framework
- [ ] Tier 6 - Wetware
  - [ ] Circuit
  - [ ] Processor
  - [ ] Processor Array
  - [ ] Framework

</details>

<details>
  <summary>Thermal Expansion</summary>

#### Thermal Expansion Upgrade

- [x] Basic
- [x] Hardened
- [x] Reinforced
- [x] Signalum
- [ ] Enderium

</details>

<details>
  <summary>Microverse Mining</summary>

#### Microverse Projection

- [x] Small Universe
  - [x] Tier 1 Microminer - Steel Plated
  - [x] Tier 2 Microminer - Titanium Plated
  - [ ] Tier 3 Microminer - Tungsten Carbide Plated
- [ ] Medium Universe
  - [ ] Tier 4 Microminer - Signalum Plated
  - [ ] Tier 5 Microminer - Iridium Plated
  - [ ] Tier 6 Microminer - Enderium Plated
- [ ] Large Universe
  - [ ] Tier 7 Microminer - Draconium Plated
  - [ ] Tier 8 Microminer - Crystal Matrix Plated
  - [ ] Tier 9 Microminer - Eternium Plated
  - [ ] Tier 10 Microminer - Neutronium Plated

</details>

### Resource Generation

<details>
  <summary>Deep Mob Learning</summary>

#### Loot Fabricating

- [x] Iron
- [x] Copper
- [x] Tin
- [x] Lead
- [x] Silver
- [x] Gold
- [x] Coal
- [x] Redstone
- [x] Glowstone
- [x] Diamond
- [x] Ender Pearl
- [x] Blaze Rod
- [ ] Basalz Rod
- [ ] Blitz Rod
- [ ] Blizz Rod
- [ ] Obsidian Dust

</details>

<details>
  <summary>Cobbleworks</summary>

- [x] Cobblestone
- [x] Stone
- [x] Polished Stone
- [ ] Stoneburnt
- [ ] Rainbow Stone
- [x] Gravel
- [x] Sand
- [x] Clay
- [ ] Aluminium
- [x] Silicon
- [x] Sodium (Hydroxide)
- [x] Lithium
- [x] Glass
- [x] Netherquartz
- [x] Fused Quartz
- [ ] Conduit Binder

</details>
