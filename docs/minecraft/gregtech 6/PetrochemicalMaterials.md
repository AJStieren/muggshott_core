# Petrochemical Materials

GT6 has very one dimensional oil processing in which oils are processed directly into various fuels. This process occurs in a distillation tower resulting in a number of fuels, lubricant, and two solid crafting materials.

## Oils

The oil types come in varieties which vary based on weight and therefore composition. While each 50L unit results in a tiny pile of parafin wax and asphalt dust, a variance is introduced for the fluids produced.

|           | Very Heavy Oil | Heavy Oil | Light Oil | Raw Oil | Oil | Soulsand Oil |
| --------- | -------------- | --------- | --------- | ------- | --- | ------------ |
| Lubricant | 100            | 80        | 25        | 50      | 50  | 80           |
| Petrol    | 35             | 30        | 15        | 25      | 25  | 10           |
| Propane   | 10             | 15        | 50        | 25      | 25  | 10           |
| Butane    | 10             | 15        | 50        | 25      | 25  | 10           |
| Fuel      | 70             | 60        | 25        | 50      | 50  | 20           |
| Diesel    | 45             | 35        | 15        | 25      | 25  | 10           |
| Kerosine  | 40             | 35        | 15        | 25      | 25  | 10           |


## Fuels

The fuel types come in both fluid and gas form. Petrol, Propane, Butane, and Fuel all have continued processes beyond distillation, but the remaining exist solely for consumption. Each liter of fuel has a set amount of GU (generic unit) to relay the amount of energy expected from the substance. The burning value refers to use in burning boxes while the engine value refers to direct consumption in the form of diesel engines or turbines. Despite having a value correlating to engines, not all fuels can be used in that manner.

|         | Petrol | Propane | Butane | Fuel | Diesel | Kerosine |
| ------- | ------ | ------- | ------ | ---- | ------ | -------- |
| Burning | 320    | 384     | 384    | 384  | 320    | 320      |
| Engine  | 448    | 512     | 512    | 512  | 448    | 448      |

## Efficiency

With the combination of GU value and the distillation result of the various oils, we're capable of generating an expected amount of energy per liter of oil when processed.

|         | Very Heavy Oil | Heavy Oil | Light Oil | Raw Oil | Oil  | Soulsand Oil |
| ------- | -------------- | --------- | --------- | ------- | ---- | ------------ |
| Burning | 1459.2         | 1331.2    | 1248      | 1248    | 1248 | 499.2        |
| Engine  | 1996.8         | 1817.6    | 1683.2    | 1696    | 1696 | 678.4        |
