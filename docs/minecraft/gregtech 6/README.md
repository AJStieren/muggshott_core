# muggshott_core: GT6

Gregtech 6 is a singular mod that is so comprehensive entire modpacks aspire to that level of change.

## TODO

<ul class="task-list">
    <li class="task-list-item">
        <input type="checkbox" checked="">
        Document machine processes
    </li>
    <li class="task-list-item">
        <input type="checkbox" checked="">
        List material values    
    </li>
    <li class="task-list-item">
        <input type="checkbox" checked="">
        Break down energy types    
    </li>
    <li class="task-list-item">
        <input type="checkbox" checked="">
        Break down material processes    
    </li>
</ul>

[ ] Document machine processes<br>
[ ] List material values<br>
[ ] Break down energy types<br>
[ ] Break down material processes<br>
