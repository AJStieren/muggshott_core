# Farm

## Generic Farm

9x9 tilled soil farm

Applicable Plants:

* Wheat
* Carrot
* Potato
* Beet
* Hops ????
* 

## Sugarcane Farm

| Tile      |       |           |
| --------- | ----- | --------- |
| Sugarcane |       | Sugarcane |
| Sand      | Water | Sand      |

## Pumpkin and Melon Farm

| Tile |      |
| ---- | ---- |
| Seed | Air  |
| Dirt | Dirt |
