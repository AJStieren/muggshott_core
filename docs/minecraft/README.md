# Minecraft

Modpacks:

* [Compact Claustrophobia](compact/README.md)
* [Gregicality Skyblock](gregicality_skyblock/README.md)
* [Gregtech 6](gregtech%206/README.md)
* [Omnifactory](omnifactory/README.md)
* [Stoneblock 2](stoneblock%202/README.md)
