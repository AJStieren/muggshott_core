# Compact Claustrophobie


## TODO

### Compact Rooms

- [x] Tiny : 3x3x3
- [x] Small : 5x5x5
- [x] Normal : 7x7x7
- [x] Large : 9x9x9
- [ ] Giant : 11x11x11
- [ ] Maximum : 13x13x13

### Automations

#### Auto-Clickers

- [ ] Stone Pick
- [ ] Iron Pick
- [ ] Steel Pick
- [ ] Diamond Pick
- [ ] Aluminium Pick

#### Material Processes

- [ ] Obsidian
- [ ] HOP Graphite
- [ ] Phyto-gro
- [ ] Sewage
- [ ] TNT
- [ ] Grassoline
- [ ] Steel
- [ ] Sieve
- [ ] Clay
- [ ] Cryotheum
- [ ] XP
