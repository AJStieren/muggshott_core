# Personal

Personal documentation often consisting of thought processes that are independent of more consolidated efforts which have their own section.

## Journal (No More)

The journal is following the Bujo track which I used to keep on paper. This "pseudo-bujo" is intended to fill the niche of needing a place to write down daily activities and minor thoughts. 


NOTE: This has been scrapped. Rather than continuing through a digital journaling method which can be easily ignored, I'm electing to continue to use pen and paper. Something about the physical interaction with the pages makes me feel accountable to some degree.
