# Desktop Customization

A reform of my Manjaro desktop aesthetics is in order. Part of this is a general revision of appearance on the whole, but separate virtual desktops are also in order to delineate different active work spaces. The desired changes can be split into cosmetic and functional where the former is in the majority.

## Cosmetic

Cosmetic changes are not functional in nature. While they may have some amount of nominal change to the existing functions of the desktop, no new functionality will be put in place.

### Standalone Clock

### Dock

A Latte Dock implementation will have an aesthetic close to my work laptop.

### Icon Revision

Revise the icons to a more rounded form in order to closer match the Latte Dock aesthetic.

### Font

General font changes to roboto for a relativel uniform look.

## Functional

### Virtual Desktops

Virtual Desktops need a simple keyboard shortcut to go to and from.

### Sound Visualizer

This is grossly unnecessary, but a visualizer for the sound just seems conceptually cool.
