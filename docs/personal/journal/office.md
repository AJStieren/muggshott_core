# Office

* Stuff
  * To Sort
    * Computer
      * Desktop
      * Monitor
      * Speakers
  * Desk
  * File Cabinets
  * Hutch
  * Network Rack
  * Cabinet Shelf
  * Floating Shelf
* Cable Management

## Stuff

### To Sort

These things do not have a dedicate home at this time.

### Desk

Stuff in the Desk

### File Cabinets

The bottom cabinets beneath the display case/hutch directly behind the desk.

### Hutch

This is the glass cabinet display on top of the filing cabinets behind the desk

### Network Rack

The Rack sits to one side of the filing cabinets due to the proximity to the Google Fiber jack in the wall.

### Cabinet Shelf

This shelf system sits on the wall opposite of the network rack to maintain symmetry

### Floating Shelf

This has yet to be installed but will likely find a home directly above the network rack

## Cable Management

Managing the cables will likely be a pain.

### Ideas

The premise of downsizing to a single (large) monitor presents the opportunity to run a single power and DP cable for all visual needs. With a mounted power brick below the desk, that would further reduce the number of cables needing to run across the floor. The question remains how to deal with sound and peripherals.