# Raspberry Pi Cluster

The [Self Hosting Blog](https://theselfhostingblog.com) has a [tutorial](https://theselfhostingblog.com/posts/setting-up-a-kubernetes-cluster-using-raspberry-pis-k3s-and-portainer/#) for spinning up a k3s cluster with portainer.

## Pre-requisites

**Physical Equipment Purchases**

* Raspberry Pi 4
* Network Switch
* 3 Ethernet cables
* Modular Pi rack
* 2 SD cards

