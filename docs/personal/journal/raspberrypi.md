# Raspberry Pi

This is a document concerning my progress with the existing Raspberry Pi. I intend to get more and need to have easily duplicatable steps with them to ensure that they behave properly when installing the k3s cluster.

## Breakdown

The steps for making the homelab functional:
1. Create the initial Pi OS (Ubuntu Server)
2. Install Kubernetes
3. Define the static networking namespace
4. Repeat steps 1-3 for all Pis
5. Define the administrator node and worker nodes
6. Start the cluster from the administrator node
7. Have the worker node join the worker nodes
8. Configure local Kubernetes to use the cluster daemon
9. Deploy the homelab

## Getting Started

### MicroSD

A MicroSD card needs to be formatted with the appropriate OS before the initial boot of the Pi. Use the Raspberry Pi Imager (RPI) or Imager program which will give a set of options with which to format the SD Card.

#### Operating System

Other General Purpose -> Ubuntu -> Ubuntu Server 20.04.02 LTS (RPi 2/3/4/400)

#### Storage

The MicroSD Card mounted on the system. Once the write is complete the card will be ejectable.

### Pi Initial Run

```bash

# Update the pi
sudo apt update
sudo apt upgrade

# Install the docker.io package
sudo apt install -y docker.io

# Create or replace the contents of /etc/docker/daemon.json
# Enable cgroup driver
sudo cat > /etc/docker/daemon.json <<EOF
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "log-driver": "json-file",
    "log-opts": {
        "max-size": "100m"
    },
    "storage-driver": "overlay2"
}
EOF

# Append the cgroups and swap options to the kernel command line
# Note the space before "cgroup_enable=cpuset", to add a space after the last existing item on the line
sudo sed -i '$ s/$/ cgroup_enable=cpuset cgroup_enable=memory cgroup_memory=1 swapaccount=1/' /boot/firmware/cmdline.txt

# Enable net.bridge.bridge-nf-call-iptables and -iptables6
cat <<EOF | sudo tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sudo sysctl --system

# Add the packages.cloud.google.com atp key
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

# Add the Kubernetes repo
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF

# Update the apt cache and install kubelet, kubeadm, and kubectl
# (Output omitted)
sudo apt update && sudo apt install -y kubelet kubeadm kubectl

# Disable (mark as held) updates for the Kubernetes packages
sudo apt-mark hold kubelet kubeadm kubectl
```
