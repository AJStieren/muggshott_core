# Techno Tim k3s

[Techno Time Tutorial](https://www.youtube.com/channel/UCOk-gHyjcWZNj3Br4oxwh0A) for [k3s in minutes](https://www.youtube.com/watch?v=UoOcLXfa8EU)

## Proposed Physical Infrastructure

2 Raspberry Pi 4's 2GB RAM - server nodes
4 Raspberry Pi 4's 8GB RAM - worker nodes
6 Raspberry Pi 4 POE Hats
1 8-port unmanaged POE switch
Cluster Case
7+ Ethernet Cables

## Architecture

1 Load balancer -> 2 k3s server -> 4 workers

### Load Balancer

This is an NGinx

nginx_lb/nginx.conf
```json
events {}

stream {
    upstream k3s_servers {
        server 192.168.60.20:6443;
        server 192.168.60.21:6443;
    }

    server {
        listen 6443;
        proxy_pass k3s_servers;
    }
}
```

## Thoughts

I'm wondering on the efficacy of this loadout. What's the purpose behind it in the long run to have a load balancer which is itself still a single point of failure? Can the load balancer itself be made self-healing?
