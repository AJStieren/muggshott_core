# Terell Wallace

Faction: Caldari
Corporation: Karmafleet University
Clone Status: Omega
Designation: Main

## Skills

### Armor

Hull Upgrades IV
Mechanics V
Repair Systems II

### Drones

Drone Avionics III
Drone Sharpshooting
Drones IV
Light Drone Operation II
Mining Drone Operation I

### Electronics Systems

Electronic Warfare III
Propulsion Jamming I

### Engineering

Capacitor Emission Systems III
Capacitor Management III
Capacitor Systems Operation III
CPU Management IVQ
Electronics Upgrades IV
Energy Grid Upgrades III
Power Grid Management V
Weapon Upgrades II

### Fleet Support

Leadership III
Mining Foreman III

### Gunnery

Controlled Burst III
Gunnery IV
Medium Hybrid Turret II
Motion Prediction III
Rapid Firing III
Sharpshooter III
Small Energy Turret III
Small Hybrid Turret IV
Surgical Strike III
Trajectory Analysis II

### Missiles

Light Missiles II
Missile Launcher Operation III
Rapid Launch II

### Navigation

Acceleration Control I
Afterburner III
Evasive Maneuvering III
High Speed Maneuvering I
Navigation IVQ
Warp Drive Operations III

### Neural Enhancement

Biology II
Cybernetics I

### Planet Management

Command Center Upgrades II
Interplanetary Consolidation II
Planetology II
Remote Sensing III

### Production

Advanced Industry I
Industry V
Mass Production I

### Resource Processing

Astrogeology III
Gas Cloud Harvesting III
Mining IVQ
Mining Upgrades II
Reprocessing III
Salvaging III

### Rigging

Astronautics Rigging II
Jury Rigging III

### Scanning

Archaeology III
Astrometric Acquisition III
Astrometric Pinpointing I
Astrometric Rangefinding III
Astrometrics IV
Hacking III
Survey III

### Science

Science IV

### Shields

Shield Emissions Systems I
Shield Management II
Shield Operation II
Shield Upgrades II
Tactical Shield Manipulation II

### Social

Social II-

### Spaceship Command

Amarr Frigate II
Caldari Cruiser II
Caldari Destroyer III
Caldari Frigate III
Caldari Industrial I
Gallente Frigate I
Mining Barge IIIQ
Mining Frigate III
Minmatar Frigate II
Spaceship Command III

### Structure Management

Anchoring II

### Targeting

Long Range Targeting III
Signature Analysis III
Target Management III

### Trade

Marking IQ
Trade II

## Implants
