# Infrastructure Migration

We're migrating the infrastructure from on premises to being an AWS Cloud base. This will involve upgrading the tech stack, moving the infrastructure and config to a containerized format, and codifying as many elements as possible.

## __TOC__

1. Existing Infrastructure
2. Proposed Infrastructure
3. Terraform Provisioning
4. Application Strategy

## 
