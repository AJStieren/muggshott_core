# CICD

We're officially going for AWS bringing in that framework to the CICD pipeline.

## DevOps

### Steps

* Plan
* Code
* Build: Maven/Docker
* Test: Junit
* Release: ????
* Deploy:
* Operate
* Monitor

#### Legacy

On legacy we use the following technologies for the associated DevOps steps:

Code: Github (Public and Enterprise)
Build: Maven/Gradle
Test: JUnit
Release: ~~Jenkins~~
Deploy: Ansible/Puppet

#### Modern

Modern CICD patterns will largely rely on the same code infrastructure as before, however the integration layers and deployments can be modernized.

Code: Github (Public and Enterprise)
Build: Docker
Test: JUnit
Release: Jenkins
Deploy: EKS/Kubernetes/Helm

## AWS

A list of the potential AWS components to run the CICD pipeline (Gitops?)

* Elastic Beanstalk
* S3
* Code Pipeline
* EKS/Fargate

### Elastic Beanstalk

Elastic Beanstalk has components which match up perfectly with our existing

## External

A list of the potential components outside of AWS likely to be used for the CICD implementation

* Artifactory
* Jenkins???
* Github Actions
