# AWS Node Metrics

## Current Prod Node

2 CPU - 2.4 GHz
8 GB RAM

### Internal

12 CPU - 2.4 GHz
48 GB RAM

### External

4 - 2.4
16 GB RAM

### Total

16 CPU - 2.4 GHz
64 GB RAM

## Projected

RAM | Node | Container Counter
---- | ---  |
3 GB | Repose | ???? MAX: 8
3 GB | Atom Hopper | Repose/2
1 GB | Catalog | 1 maybe 2

8 CPU - 2.4 GHz
4 CPU - 2.4 GHz
32 GB

Worst Case Scenarios: DFW and SYD

### Worst Case

8 CPU 2.4 GHz
32 GB

### Nominal Case

4 CPU - 2.4 GHz
32 GB

### Cheaping Out

4 CPU - 2.4 GHz
16 GB

### Testing

2 CPU - 2.4 GHz
8 GB

Environment | Region | CPU | Memory | AWS Node
----------- | ------ | --- | ------ | --------
Production  | ORD    | 8   | 32 GB  | 
Production  | DFW    | 8   | 32 GB  |
Production  | IAD    | 4   | 16 GB  |
Production  | HKG    | 4   | 16 GB  |
Production  | LON    | 4   | 16 GB  |
Production  | SYD    | 4   | 16 GB  |
Staging     | US     | 8   | 32 GB  |
Staging     | UK     | 4   | 16 GB  |
Test        | US     | 2   | 8 GB   |

Node definition example: t3.medium

Dynamo DB: 

* Do we need formal declaration of size?
  * Will Archiving count against that? No...
  * If not, where do we archive? s3 bucket 1 year, glacier 7+

* dfw_automation_2021-05-10.gz
* dfw_glance_2021-05-10.gz
* dfw_autoscale_2021-05-10.gz
* dfw_identity_2021-05-10.gz
* dfw_backup_2021-05-10.gz
* dfw_keep_access_2021-05-10.gz
* dfw_bigdata_2021-05-10.gz
* dfw_lbaas_2021-05-10.gz
* dfw_billing_2021-05-10.gz
* dfw_monitoring_2021-05-10.gz
* dfw_cbs_2021-05-10.gz
* dfw_netdevice_2021-05-10.gz
* dfw_core_2021-05-10.gz
* dfw_neutron_2021-05-10.gz
* dfw_customer_2021-05-10.gz
* dfw_newrelic_2021-05-10.gz
* dfw_dbaas_2021-05-10.gz
* dfw_nimbus_2021-05-10.gz
* dfw_dcx_2021-05-10.gz
* dfw_nova_2021-05-10.gz
* dfw_dedicatedvcloud_2021-05-10.gz
* dfw_payment_2021-05-10.gz
* dfw_demo_2021-05-10.gz
* dfw_queues_2021-05-10.gz
* dfw_dns_2021-05-10.gz
* dfw_rackspacecdn_2021-05-10.gz
* dfw_domain_2021-05-10.gz
* dfw_servermill_2021-05-10.gz
* dfw_ebs_2021-05-10.gz
* dfw_servers_2021-05-10.gz
* dfw_emailapps_msservice_2021-05-10.gz
* dfw_sites_2021-05-10.gz
* dfw_emailapps_usage_2021-05-10.gz
* dfw_ssl_2021-05-10.gz
* dfw_encore_2021-05-10.gz
* dfw_support_2021-05-10.gz
* dfw_feeds_access_2021-05-10.gz
* dfw_usagesummary_2021-05-10.gz
* dfw_files_2021-05-10.gz
* _SUCCESS 

Data: 100MB - 1000 MB ~400 GB -> 100-200 GB
Data Uncompressed: 3 GB - 15 GB
Archive needs to be accessible to GDO/Informatica

Archive: 1 year to s3
Glacier: 7 year


Ideal Migration:
LB -> Repose -> AH -> Postgres
   -> Repose -> AH -> DynamoDB

Marker UUID

Alternative Migration:
Data Transfer : Postgres -> DynamoDB
New Load Balancer : vip cutoff (downtime)

Do we need to update the Load Balancer???
Question for Netsec/Jason

If new Load Balancer:

* Can we still mirror traffic?
* Do we need to have downtime for the vip cutoff?
