# Milestones

## POC

The capacity to create and deploy the Cloud Feeds service as a conceptual unit to the EKS Cluster/Nodes

## DynamoDB Implementation

DynamoDB is going to be an architectural shift for the application, so usage from the Cloud Feeds side needs to be proven. This milestone will include the provisioning of the DynamoDB, necessary definitions for the Atom Hopper DB equivalent of the PSQL DLL, and integrated connection from POST to GET.

Note: Archive definitions will be required at a later date, this milestone requires the continued support for 72 hour event lifespans

## Stateful Deployment

Service definition on deployment encompassing region, node requirements, database integrations, and external service integrations.

Note: External service integrations need to be defined, not functional for this milestone. In those cases, a mock is sufficient.

## Migration Strategy

Due to the complilcation in Markers with migration, we need to have a strategy in place with sufficient stack behind the effort to ensure minimal disruptions.

## RXT Service Connectivity

Connect Cloud Feeds to the relevant RXT Services necessary for functionality. This is primarily the Identity connection.

This Milestone will be done in coordination with the upcoming "Cloud Infrastructure Team"

## Terraform Provisioning

Able to create the expected EKS environment on a per DC basis through Terraform. This will include a dedicated Cloud Account, regional variables, node sizes (variables for adjustment if necessary), and DB connection if DynamoDB is used.

This should also include usable outputs from the plan/apply stages

## Archiving

????
