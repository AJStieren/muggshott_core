# Cloud Migration

## Containerization

Building the capacity for the component applications to be made into containers. This will require defining the components at the service level rather than having entire server instantiations for them. The majority of the component applications for Cloud Feeds are stateless allowing for the services to scale completely indepedent of each other without the risk of introducing race conditions. An asynchronous read-write relationship between Atom Hopper and what is currently a PostgreSQL DB will allow for the separate authentications from Repose on internal and external endpoints to have an obfuscated shared service layer to Atom Hopper.

TBD: Do we need to differentiate on the catalog between internal or external?
Hypothesis: No, the Catalog should return a state based on the credentials presented rather than anything regarding the endpoint used when making the request. This makes that part of the application independent of the vip as well.
 
### Atom Hopper

Atom Hopper is the Open Sourced component which we have specifically configured for our needs to utilize Apache Abdera for format validation. This is the component which interacts with the database. As the application operates through a series of adapters to specialize in each database, the configuration is more important for determining the interface as to which where our compatibility limitations are.

### Cloud Feeds Atom Hopper

### Cloud Feeds Catalog

Cloud Feeds Catalog returns a list of the feeds available to the user based on the credentials provided. This is a stateless transaction entirely determined by the POST headers.

### Repose

Repose is the front service validating credentials from users. There is a differentiation between the internal and external node configuration on legacy (live version) which will need to be duplicated in the Cloud.

## Kubernetes Deploy

While ECS and EKS are both possible given our desire to move to AWS as the primary cloud provider, using Kubernetes grants us the flexibility to transition to any cloud. While EKS will be managing the infrastructure making that aspect of the application dependent on AWS, it's not to the degree that we would have to rewrite our containerization as would be the case for ECS.

### External DB

Dynamo DB is our targeted database to transition from the old Postgres 9. This transition grants us new DB administration capabilities such as TTL events as well as the possibility of handing off management to an entire DBA team which is wholly independent from us.

### Cloud Feeds Services

The component applications of the Cloud Feeds product will require service obfuscation for the Kubernetes deployment. This obfuscation plays well with the stateless nature most of the applications have and allwos for the possibility of independent scaling of the necessary components.

Product services maintained by the dev team were mentioned above in the Containerization. These services will be the scaling factors and have the most changes during development:

* Atom Hopper
* Cloudfeeds Catalog
* Repose: Internal
* Repose: External

In addition to the product services, additional modules will be included to add diagnostic and montioring functionalities.

#### Datadog

Datadog as a service will grant the capacity to examine pod metrics. Log collection will also be streamed to the Datadog instance granting the team transparent visiblity to all levels of operations for Cloud Feeds.

### Image Repo

JFrog Artifactory

### Connect EKS Nodes

Major dependency: We need a dedicated Cloud Account for the application
Adendum: We might need a Test Cloud Account for Feeds Team and Staging/Prod Account for Ops on final revision

### Connect RXT Identity

Staging Identity could be an issue on EKS

### Set LB URL

Get Netsec/Ops

## Logging & Monitorings

### Datadog Log Collection

### Datadog Node & Pod Metrics || Grafana - Prometheus

### Archiving - DynamoDB Config

## Terraform & Provisioning

### EKS Manifests

Generic EKS Manifest with variable fillables on region, node type, etc.

### Regional Variables

* Region definitions
* Environments - Test, Staging, and Prod
* Node definitions on per DC level

### Terraform EKS Output

Provisioning should result in valid Kubectl credentials

### DynamoDB Manifest & Init

### Terratest Impl

Check the plan for the following:

* Environment Definitions
  * Prod
    * ORD
    * DFW
    * IAD
    * LON
    * HKG
    * SYD
  * Staging
    * US (ORD)
    * UK (ORD)
  * Test
* DynamoDB
* Datadog Integrations

### CICD Hooks



## CICD

### Jenkins Maser & Slaves

### Image Pipelines

Automatic check and publication on Master

* Atom Hopper
* Repose
* Catalog

### Unit Testing

### Integration Test

This is a revision of the existing Regression test cases

Run on a schedule

### Artifact Release

Publish the final artifact to JFrog

### Deployment Pipelines

## Documentation

All the things. Yay....
