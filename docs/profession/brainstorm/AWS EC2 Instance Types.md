# EC2 Instance Types

While the goal of the migration is to go to an EKS cluster, any proposal will have to be expressed in EC2 instance types in order to get a proper bid. Given the long term commitment intent for the Cloud Feeds application, it is probable that a discounted price will be granted making it even more important to express the 

## General Purpose

### T3

T3 instances are the next generation burstable general-purpose instance type that provide a baseline level of CPU performance with the ability to burst CPU usage at any time for as long as required. T3 instances offer a balance of compute, memory, and network resources and are designed for applications with moderate CPU usage that experience temporary spikes in use.

T3 instances accumulate CPU credits when a workload is operating below baseline threshold. Each earned CPU credit provides the T3 instance the opportunity to burst with the performance of a full CPU core for one minute when needed. T3 instances can burst at any time for as long as required in Unlimited mode.

| Instance   | vCPU* | CPU Credits/hour | Mem (GiB) | Storage  | Network Performance (Gbps) |
| ---------- | ----- | ---------------- | --------- | -------- | -------------------------- |
| t3.nano    | 2     | 6                | 0.5       | EBS-Only | Up to 5                    |
| t3.micro   | 2     | 12               | 1         | EBS-Only | Up to 5                    |
| t3.small   | 2     | 24               | 2         | EBS-Only | Up to 5                    |
| t3.medium  | 2     | 24               | 4         | EBS-Only | Up to 5                    |
| t3.large   | 2     | 36               | 8         | EBS-Only | Up to 5                    |
| t3.xlarge  | 4     | 96               | 16        | EBS-Only | Up to 5                    |
| t3.2xlarge | 8     | 192              | 32        | EBS-Only | Up to 5                    |

**Features:**

* Burstable CPU, governed by CPU Credits, and consistent baseline performance
* Unlimited mode by default to ensure performance during peak periods and Standard mode option for a predictable monthly cost
* Powered by the AWS Nitro System, a combination of dedicated hardware and lightweight hypervisor
* AWS Nitro System and high frequency Intel Xeon Scalable processors result in up to a 30% price performance improvement over T2 instances

## Memory Optimized

Memory optimized instances are designed to deliver fast performance for workloads that process large data sets in memory.


