# Rancher Presentation

I have been asked to give a Rancher Brown Bag session as another team is aiming to use it as the basis for a UI implementation for their product as a service. Personally, I've done a nominal amount of research into the subject for my homelab, but implementation has yet to occur. I mean, I done a small deployment of it, but not a real genuine use case.

They have been pushed off for the time being under the premise that my time is a little booked due to the upcoming Rax.IO, deployments of 1.138.1, and catching up to the current state of development after a week off.

## Preperation

In order to prepare for the presentation, I would need the following elements to some degree:

* Practical experience
* Working example
* Personal knowledge

### Practical Experience

The practical experience is pretty straight forward: make my Raspberry Pi homelab which I intended to do anyway. I've been procrastinating on it, but the reality is I've done a considerable amount of research and planned out how to implement the cluster. I have my examples and sources lined up, so I need to buy the materials. In addition, most of the expected but variable expenses have been taken care of for the time being so it's unlikely that the spending the money on the lab would be burdensome unless there is an emergency.

After the pi cluster is installed and initiated with k3s, Rancher can be installed. With Rancher, I would need a set of deployments to manage before I can say I have personal "experience".

### Working Example

The issue with a working example is that my implementation would be a pure containerized version that I'm simply using. Customizing the software on its face is something I have no intention of doing, but instead using its innate features to manage the homelab. In addition, I would need something to manage which begs yet another question: what do I want to show? Ideally it would be something everyone is familiar with in concept, but there is also

 a question of utility.

#### Git Repo

A git repository is easily one of the most recognizable applications in our industry. There is no mistaking its purpose or how it functions. A database that houses your code with version control. The problem is that many don't see the point in having a locally hosted git repository. In my case, it's nice to have a secure method of maintaining code bases locally so I can keep my changes between machines. I could and actually do use a service for that much of the time, but having a local repository mirror and external one means that I'm never without a version of it on any of my machines within the LAN.

#### Minecraft Server

This... is play time. There are no two ways around it, this is admitting that I'm literally using Rancher as a toy. It taints the entire ordeal.

#### Heimdall

Heimdall is technically a portal to other applications, but it's also a tool for exposing them to external sources without having to use port forwarding. In a manner of speaking, it's a security measure for allowing people to access my services from the outside world (namely myself).

#### Theia IDE

I am already a known advocate for cloud based development environment. Containerized development environments? Environments as code. Let's go with that. Having a Theia implementation or perhaps multiple across a number of repositories could not only make the case for a local host (it allows for remote development anywhere in the world provided internet access is available to both myself and the house), but it would also illustrate how having Rancher can simplify hosting such services in parallel or on demand.

#### Network Attached Storage (NAS)

I can't show this.... I use a proprietary NAS.

#### Factorio Server

See Minecraft

#### PlantUML Renderer

This one is a little more... niche, but the fact that we kinda use it with Cloud Feeds makes it a bit more pallatable. Let's face it, docs as code is cool.

#### Cloud Feeds

This is the obvious choice. We're currently working on Cloud Feeds. Having a deployment of Cloud Feeds on my k3s cluster and showing it through Rancher directly takes the whole demonstration to the enterprise level. What's more, I can do this on a single 8GB Pi 4 allowing me to have my cluster separate in a post experience world and still have a single node cluster separate from it to show Rancher and Cloud Feeds in a vacuum.

### Personal Knowledge

I know the basics of what Rancher does, but only in the capacity that I know what kubernetes does. Unfortunately, I have not and have no interest in digging into the guts of the application. The guts is what the Repose team will need in order to get any real benefit from a brown bag session.
