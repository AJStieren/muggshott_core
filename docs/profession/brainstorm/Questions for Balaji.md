# Balaji 1:1 Questions

## Professional

Issues with work

### TCT Mentorship

I am obviously unable to apply to be a mentor as I'm not at the top of my technical career track (3 vs 4/5), but would being a mentee help me in the long run? I'm not particularly concerned with needing help to expand my technical skills as I'm self-motivated to make those improvements on my own and in my own time outside of work; however, I'm not sure how to improve in the "soft skills" necessary to progress beyond my current scope as an individual contributor with slight lead responsiblities.

### Fires are out

Ballista has a configuration solution so the onus is no longer on us. Or even the dev team as Straw is now capable of making the change unilaterally.

### Cloud Accounts for AWS on Cloud Feeds:

We're still under the initial licenses provisioned by Justin. It's cheating and we're holding back to make sure we're not having any budget impacts. There has not been an impact on our development yet, but we're going to need to make that jump into Terraform provisioning in the next quarter or so if we're going to come even close to our migration timeline.

### AWS Architect plz?

We're cobbling together the necessary information for a truly informed proposal on AWS product commitment, but we're going to eventually need a consultation with someone on the specifics.

**Products Expected:**

* EKS - Elastic Kubernetes Service
* Fargate - ????
* DynamoDB
* EBS - Fallback for DynamoDB
* Archiving (Glacier and EBS or dedicated)

### Migration

We've discussed migration strategies, but Straw poked a hole in what was supposed to be our zero impact transition. Markers are random UUIDs generated upon publication so mirroring the Post events through a load balancer will result in different markers from different Atom Hoppers.

## Personal

Life happens

### Headache Institute
