# RSI EKS

Rather than use a proper AWS instance, we're having to use the RSI implementation. RSI uses a Rancher interface to manage the EKS cluster, gives namespace separation with user permissions differentiated by SSO, and the standard API webhooks involved in the process. However, the catalog and several other features are disabled from the base version of Rancher.

## __TOC__

* Storage Solutions

## Storage Solutions

While dedicated persistent storage is available within the cluster, the DynamoDB and S3 bucket solutions initially envisioned are not able to be provisioned within the Rancher interface. With that in mind, configuration of the database though the helm chart/dockerfile should be trivial. The issue is the act of privisioning itself.

### Dynamo DB

The Dynamo DB instance 

### S3

An S3 bucket will be the first stage in the archiving process. Entries for a limited number of feeds will be translated into a format approximating the existing HDFS storage solution that Ballista translates them to.

### Glacier

A Glacier instance will be the final resting place for archived events.

## 
