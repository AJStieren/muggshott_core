# Brainstorm

There is little to no form or factor to this. It's a genuine brain dump.

* [AWS Instances](AWS%20EC2%20Instance%20Types.md)
* [AWS Node Metrics](AWS%20Node%20Metrics.md)
* [Cloud Migration](Cloud%20Migration.md)
* [Marker Migration](Marker%20Migration.md)
* [Milestone](Milestones.md)
* [1:1 Questions](Questions%20for%20Balaji.md)
* [Rancher Brown Bag](Rancher%20Presentation.md)
* ~~Reorg~~
