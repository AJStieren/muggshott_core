# AWS vs GCP

Moving over to GDO from TES subjects Cloud Feeds to the project goals of the other organization. While Cloud Migration is likely if not mandated to continue, GDO has pursued GCP migration during the same period in which we have been evaluating AWS. *Most* of the Cloud Feeds work has been cloud agnostic by keeping the application within the container paradigm deploying to a Kubernetes server. With EKS and GKE available on each of its respective clouds, we will have to evaluate the capability of changing goals given the expertise of our new organization.

## Database

Cloud Feeds currently runs on an old PostGres server which is... terrible. Part of the Cloud Migration effort has been directed towards changing to a NoSQL database, specifically DynamoDB. DynamoDB is an AWS specific offering which offers sub-millisecond transaction rates. The GCP competitor is Datastore and likewise scales with their Kubernetes offering. 

### DynamoDB vs Datastore

According to a [benchmark of the services](https://medium.com/@wattanai.tha/nosql-cloud-service-benchmarking-dynamodb-vs-datastore-3f57b6951fb), DynamboDB has a performance advantage over Datastore even from a GCP environment making it the preferable option from an enhancement perspective. Given that moving to Datastore would require a rework of what has already been done in an effort to move Atom Hopper to being cloud ready only to achieve a lower performance results, it would be grossly inefficient to go that direction. In addition, the DynamoDB to Glacier migration is the current mechanism we intend to use to duplicate archiving functionality. Collectively, this makes DynamoDB the preference of the Dev team by a wide margin.

## CICD

Uh... this isn't solved. At all. However, the question of which cloud we're going to does raise concerns of a cloud agnostic CICD process which would push away from any integrated AWS services in the current model. With that in mind, it seems likely that we will wind up using on-premises hosted CICD.