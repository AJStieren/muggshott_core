# Me_Technical-Presentation

Develop two technical presentations to present over the year. Presentations may include but are not limited to RaxIO technical confrence talks or brown bags (within RAD corner or not).

## Category

Development

## Supports

Best Place to Work (Public)

## Due Date

March 2022

## Status

One of the following values:
5 - Exceeded
4 - Met
3 - Partially Met (Carry Forward)
3 - Partially Met (Close Goal)
2 - Not Met
1 - Not Applicable

## Completed On