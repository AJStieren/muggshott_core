 # Feeds: Containerization

Feeds needs to move to a containerized architecture. While this is an inflight feature, the implementation of container compability within the individual repos isn't enough in this case. Connectivity between Repose, Atom Hopper, Catalog, and a backend database is the goal to show true functionality and the portability of the applicationa as a whole.

 ## Category

Development

## Supports

Fanatical Customer Experience (Public)

## Due Date

5/31/21

## Status

One of the following values:
5 - Exceeded
4 - Met
3 - Partially Met (Carry Forward)
3 - Partially Met (Close Goal)
2 - Not Met
1 - Not Applicable

## Completed On