# Rackspace: Cloud Feeds Provisioning

Cloud Feeds is currently provisioned to legacy on-premises equipment. Moving from this equipment will allow TES to reclaim the boxes for more modern architecture or replace them as they have become a liability. This will also reduce the number of man hours needed by external teams for Cloud Feeds maintenance by conforming to a architecture paradigm which is no longer deprecated within the company.

## Category

Development

## Supports

Superior Financial Performance (Public)

## Due Date

????

## Status

One of the following values:
5 - Exceeded
4 - Met
3 - Partially Met (Carry Forward)
3 - Partially Met (Close Goal)
2 - Not Met
1 - Not Applicable

## Completed On