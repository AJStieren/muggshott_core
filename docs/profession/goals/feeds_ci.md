# Feeds: Continuous Integration

Feeds needs to automate many of the developer activities to give us more time to work on features. While features have been accelerating in recent months, a significant amount of time is still being forfeit to other teams and customers with minor activities. Some CI elements and actions could greatly streamline the workflow.

## Schema - Idiot Check

Schema could use an idiot check on PR submission. Whenever we get an external request for a change, we typically wind up having to do the build ourselves as a the first check before giving any real analysis to the schema itself. Running a simple `mvn clean initialize test` as a measure before reviews can be accepted would to wonders for the request process.

## Artifact Creation

Artifacts could easily be created on a full `master` branch merge. Potentially feature tagged artifacts can be used now that branch conventions have been established to allow for parallel development goals of differing elements of the application that may not release together.

## ????

## Category

Performance

## Supports

Superior Financial Performance (Public)

## Due Date

????

## Status

One of the following values:
5 - Exceeded
4 - Met
3 - Partially Met (Carry Forward)
3 - Partially Met (Close Goal)
2 - Not Met
1 - Not Applicable

## Completed On