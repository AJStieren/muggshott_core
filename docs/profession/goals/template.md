# Template

Description of the goal goes here

## Category

One of the following values:

* Development
* Performance

## Supports

This is an organization goal forwarded by the goal.

* Best Place to Work (Public)
* Fanatical Customer Experience (Public)
* Superior Financial Performance (Public)

## Due Date

## Status

One of the following values:
5 - Exceeded
4 - Met
3 - Partially Met (Carry Forward)
3 - Partially Met (Close Goal)
2 - Not Met
1 - Not Applicable

## Completed On