# Rackspace Advancement

I need to demonstrate the capacity to operate at a higher level than a Dev 3. While I've been a capable contributor in my own right, Feeds as an application needs a more formalized tech lead with aptitude for architecture and a vision for the product.

## Category

Development

## Supports

Best Place to Work (Public)

## Due Date

????

## Status

One of the following values:
5 - Exceeded
4 - Met
3 - Partially Met (Carry Forward)
3 - Partially Met (Close Goal)
2 - Not Met
1 - Not Applicable

## Completed On
