# Professional

A quick sheet to deal with professional documentation. Not that kind of professional documentation. This is kind of a knee-jerk brainstorm location currently for my work goals.

## Day to Day

* [Brainstorm](brainstorm/README.md)
* [Features](features/README.md)
* Goals
* [Scrum](scrum/README.md)

## Goals

* [Template](goals/template.md)
* [Feeds: Continuous Integration](goals/feeds_ci.md)
* [Feeds: Containerized Infrastructure](goals/feeds_container.md)
* [Personal:](goals/me_presentation.md)
* [Rackspace: Advancement](goals/rackspace_advance.md)
* [Rackspace: Present](goals/rackspace_provision.md)

