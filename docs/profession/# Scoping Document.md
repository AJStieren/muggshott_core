# Scoping Document

## System Info

System info is unchanged and screenshots are not applicable to the API (no GUI)

## ASP Config

TODO: Get a screenshot from Jason with an updated timestamp using his command.

## Github Config

Get screenshots and provide links to the repositories in the project. Also note which ones are for Production, Testing, QA, etc.

| Repository                       | Purpose    | Link |
| -------------------------------- | ---------- | ---- |
| Standard Usage Schema            | Production | Done |
| Ansible Playbook                 | Production | TODO |
| Puppet                           | Production | TODO |
| Puppet Profile                   | Production | TODO |
| Atom Hopper System Regression    | QA         | TODO |
| Atom Hopper                      | Testing    | TODO |
| Atom Hopper Cloud Feeds          | Production | TODO |
| Repose Filters                   | Production | TODO |
| Cloud Feeds Catalog              | Production | TODO |
| Cloud Feeds Ballista             | Archiving  | TODO |
| Cloud Feeds Nabu                 | Archiving  | TODO |
| Cloud Feeds Archiving Tests      | Testing    | TODO |
| Puppet Ballista                  | Archiving  | TODO |
| Puppet Profile Ballista          | Archiving  | TODO |
| Ansible Playbook Ballista Deploy | Archiving  | TODO |

### Standard Usage Schema

DONE

### Ansible Playbook

Access Restrictions prevent configuration

### Puppet

Access Restrictions prevent configuration

### Puppet Profile

Access Restrictions prevent configuration

### Atom Hopper System Regression

TODO

* Apply branch protections
* Screenshot branch protection rules

Permissions not available to see user access list

### Atom Hopper

TODO

* Screenshot branch protection rules
* Screenshot Authorized Users

### Atom Hopper Cloud Feeds

TODO

* Screenshot branch protection rules
* Screenshot Authorized Users

### Repose Filters

TODO

* Screenshot branch protection rules
* Screenshot Authorized Users

### Cloud Feeds Catalog

TODO

* Screenshot branch protection rules
* Screenshot Authorized Users

### Archiving

Archiving will fall outside of intended repository scope for the SOX Scoping document.

**Repositories**

* Cloud Feeds Ballista
* Cloud Feeds Nabu
* Cloud Feeds Archiving Tests
* Puppet Ballista
* Puppet Profile Ballista
* Ansible Playbook Ballista Deploy

## Backup

This is not a thing. Application is deployed in clustered nodes with a static configuration. Communication is with the database which is designed to remove information over time. A master-slave database is used for API data such that data is written to master and recalled from slave. By definition, backup occurs through operation as the databases are duplicates of each other.

## Scheduled Jobs

These are unchanged. No tool with a UI is used to perform these jobs.

TODO: Screenshot of Datadog