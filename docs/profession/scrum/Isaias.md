# Isaias

I'm soliciting feedback for the scrum ceremonies. I've asked for feedback before, but even from those with agile training I regularly just get told "you're doing great" which is completely unhelpful

## What I Know is Wrong

Since losing Prathibha and then Shubham, I've been letting basic practices such as Acceptance Criteria fall to the wayside. The current voting block for pointing is also messy in its organization, but when I tried to do blind cards through a service or an app there was some resistance and it felt like it would be more counterproductive to pursue.

Sprint Planning, Review, and Retro have been combined into a less productive almalgamation of the 3. Product Backlog Groom also hasn't been formalized. Team size and lack of a product owner influenced the current state of the ceremonies.

Story Formation is also poor at the moment. I'm good with broad strokes for overarching initiatives, but PI Planning without any buy in from outside sources was less than fruitful

Backlog state is bad. Yeah... I know.

## What I Think Worked

Our points are insular, but it let me get some engagement with the team to pull them into conversations that I wasn't getting before. I more or less leveraged the points to do the grooming rather than groom and then point the effort.

## Isaias Comments

### Pros

Combining ceremonies was a good thing.

### Cons

Own too much

Resistant to change - be more open

The other members of the team should be participating more rather than dictation being given to them on a task list. This is not very agile to have orders dictated by a single member of the team regardless of expertise levels. Act as a SME rather than a manager.
