# Standup

Give a series of updates rapidly during standup. Also document potential or actual follow-up topics which would land in the "parking lot".

## Updates

Updates should fall along the lines of work affiliated with stories, epics, or specific interactions/tasks which may need to take place to facilitate the former. Work which would be required outside of this likely comes outside of the scrum cycle or needs to have a story written for it.

## Parking Lot

Stuff that isn't immediately relevant to daily activities.
