# Standup - 4/30/2021

Yesterday:

* SOX Walkthrough
* Ballista behavior achieved
  * Artifact won't create

Today:

* SOX Walkthrough
  * Documentation Spotlight

The SOX Compliance walkthrough will is a review of our development processes in the form of a PowerPoint presentation which is not my native language. My part includes updating team structure, change management process, software development lifecycle, and will need notes on the compliance enforcement mechanisms we use to prevent these processes from being circumvented. 
