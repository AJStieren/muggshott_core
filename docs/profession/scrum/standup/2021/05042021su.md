# Standup - 5/3/2021

Yesterday:

* Sox compliance done afaik
* Ballista - Test Security Issue

Today:

* Ballista
  * Token Security Solve
  * Dependency Upgrade???

## Summary

Sox is taken care of and Ballista is still a pain. The issue appears to be a security issue in the test SCP server. Gonna try either upgrading the RSA token or just updating the project across the board for its dependencies. It was built for Java 1.7
