# Standup - 7/23/2021

Yesterday:

* No need to rehash the news
* Several meetings
* Ansible didn't work

Today:

* Ansible retry
* Lots of morning meetings
* Telemedicine visit
* User Permissions
