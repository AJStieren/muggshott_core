# Standup - 2/28/2022

Past:

* UML Diagram
* Doc Request
* Office Setup

### Office Setup

All of my VPN issues have yet to be solved, but I have reached the state that I can reliably run the regressions.

Present:

* Doc Request
* Repose 7.1.5.2 Release Instructions

## Parking Lot

Use the Cloud Feeds service account to upload the Repose artifact as that's what is the current impediment.
