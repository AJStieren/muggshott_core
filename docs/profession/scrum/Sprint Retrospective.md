# Sprint Retrospective

## What Did We Do Well?

* Bachu is on the team!!!
  * We hope this is a good thing in his eyes. We like him.
* PWC is taken care of
* PI Planning ate 3 half days and was dealt with minimal impact to sprint
* Repose 7.1.5.2 was built
* Action Items adequately dealt with

## What Should We Have Done Better?

* Open Nexus found to not be upstream of Ansible
* PWC is crazy. Like crazy crazy. Things that don't exist crazy.
* SOX Compliance Audit board due date misread
  * It was March 22nd not February 22nd
* Stories not sufficiently planned prior to sprint planning.

## Action Items

* @AJ Needs to get in the backlog with stuff
* @AJ make a docs as code feature in the infrastructure project for the PlantUML diagrams
* @Team Upload 7.1.5.2 to Maven Reseach Cloud
  * OR!!
* @Team Add Opennexus to the Ansible/Puppet repository check