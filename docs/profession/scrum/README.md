# Scrum

This team runs a two-week sprint with scrum elements. These scrum elements are largely in the codification of features and development work into epics, stories, and tasks as well as classifying the exploration of new technologies as spikes.

## Epics

* Containerization
* Documentation
* Quick Provisioning
* Self-Service

## Scrum Ceremonies

Daily Standup - M-F 14:10:00-!$:15:00 UTC
Weekly Sync - Thursday 14:15:00-15:00:00 UTC
Spring Planning - Alternating Wednesdays

### Standup

* [Standup - 3/19/2021](standup/2021/03192021su.md)
* [Standup - 3/22/2021](standup/2021/03222021su.md)
* [Standup - 3/23/2021](standup/2021/03232021su.md)
* [Standup - 4/12/2021](standup/2021/04122021su.md)
* [Standup - 4/13/2021](standup/2021/04132021su.md)
* [Standup - 4/14/2021](standup/2021/04142021su.md)
* [Standup - 4/15/2021](standup/2021/04152021su.md)
* [Standup - 4/16/2021](standup/2021/04162021su.md)
* [Standup - 4/19/2021](standup/2021/04192021su.md)
* [Standup - 4/23/2021](standup/2021/04232021su.md)
* [Standup - 4/26/2021](standup/2021/04262021su.md)
* [Standup - 4/28/2021](standup/2021/04282021su.md)
* [Standup - 4/30/2021](standup/2021/04302021su.md)
* [Standup - 5/3/2021](standup/2021/05032021su.md)
* [Standup - 5/4/2021](standup/2021/05042021su.md)
* [Standup - 5/5/2021](standup/2021/05052021su.md)
* [Standup - 5/6/2021](standup/2021/05062021su.md)
* [Standup - 5/7/2021](standup/2021/05072021su.md)
* [Standup - 5/10/2021](standup/2021/05102021su.md)
* [Standup - 5/11/2021](standup/2021/05112021su.md)
* [Standup - 5/12/2021](standup/2021/05122021su.md)
* [Standup - 5/13/2021](standup/2021/05132021su.md)
* [Standup - 5/17/2021](standup/2021/05172021su.md)
* [Standup - 5/18/2021](standup/2021/05182021su.md)
* [Standup - 5/19/2021](standup/2021/05192021su.md)
* [Standup - 5/24/2021](standup/2021/05242021su.md)
* [Standup - 5/25/2021](standup/2021/05252021su.md)
* [Standup - 5/26/2021](standup/2021/05262021su.md)
* [Standup - 5/27/2021](standup/2021/05272021su.md)
* [Standup - 6/7/2021](standup/2021/06072021su.md)
* [Standup - 6/8/2021](standup/2021/06082021su.md)
* [Standup - 6/9/2021](standup/2021/06092021su.md)
* [Standup - 6/10/2021](standup/2021/06102021su.md)
* [Standup - 6/11/2021](standup/2021/06112021su.md)
* [Standup - 6/18/2021](standup/2021/06182021su.md)
* [Standup - 6/21/2021](standup/2021/06212021su.md)
* [Standup - 6/22/2021](standup/2021/06222021su.md)
* [Standup - 6/23/2021](standup/2021/06232021su.md)
* [Standup - 6/24/2021](standup/2021/06242021su.md)
* [Standup - 6/25/2021](standup/2021/06252021su.md)
* [Standup - 6/28/2021](standup/2021/06282021su.md)
* [Standup - 6/29/2021](standup/2021/06292021su.md)
* [Standup - 6/30/2021](standup/2021/06302021su.md)
* [Standup - 7/1/2021](standup/2021/07012021su.md)
* [Standup - 7/2/2021](standup/2021/07022021su.md)
* [Standup - 7/6/2021](standup/2021/07062021su.md)
* [Standup - 7/7/2021](standup/2021/07072021su.md)
* [Standup - 7/8/2021](standup/2021/07082021su.md)
* [Standup - 7/19/2021](standup/2021/07192021su.md)
* [Standup - 7/20/2021](standup/2021/07202021su.md)
* [Standup - 7/21/2021](standup/2021/07212021su.md)
* [Standup - 7/22/2021](standup/2021/07222021su.md)
* [Standup - 7/23/2021](standup/2021/07232021su.md)
* [Standup - 7/27/2021](standup/2021/07272021su.md)
* [Standup - 7/29/2021](standup/2021/07292021su.md)
* [Standup - 8/2/2021](standup/2021/08022021su.md)
* [Standup - 8/3/2021](standup/2021/08032021su.md)
* [Standup - 8/4/2021](standup/2021/08042021su.md)
* [Standup - 8/5/2021](standup/2021/08052021su.md)
* [Standup - 8/6/2021](standup/2021/08062021su.md)
* [Standup - 8/9/2021](standup/2021/08092021su.md)
* [Standup - 8/10/2021](standup/2021/08102021su.md)
* [Standup - 8/11/2021](standup/2021/08112021su.md)
* [Standup - 8/12/2021](standup/2021/08122021su.md)
