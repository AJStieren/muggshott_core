# Sample

This is a test of the mermaid extension for VS Code. If successful, I will probably remove this anyway, but duck it.

## Graph

This is a regular graph translated into a flow chart by Mermaid

```mermaid
graph TD
    A --> B
    A --> C
    B --> D
    C --> D
```

## Gantt
This is a Gantt diagram taking text and creating a full project timeline with svg.

```mermaid
gantt
    title Product Roadmap
    dateFormat YYYY-MM-DD
    section AWS Migration
    A task              :a1, 2022-02-25, 5d
    Another task        :after a1, 2d
    section CICD Automation
    Regression Pipeline :b1, 2022-02-27, 10d
    Schema Pipeline     :after b1, 5d
    section Infra Migration
    Something To Do     :c1, 2022-03-01, 12d
```

## Sequence Diagram

You know the drill at this point

```mermaid
sequenceDiagram
    participant Client
    participant You
    participant Me
    Client ->> You: Request
    You ->> Me: Request
    Me ->> You: Reponse
    loop
        Me ->> Me: DB Query
    end
    Me -->> Client: Response
    You -->> Client: Response
```

## Git Graph

When describing what exactly? I'm genuinely not sure.

```mermaid
gitGraph
    commit
    commit
    commit
    commit
    commit
    branch feature
    commit
    commit
    checkout main
    commit
    checkout feature
    commit
    merge main
    checkout main
    merge feature
```
## Some rambling bullshit?

I don't know that I'm doing at the moment.

```mermaid
graph LR
    X --> Y
    X --> Z
    Y -.-> Fin
    Z ==> Fin
```