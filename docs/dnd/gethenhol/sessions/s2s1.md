# Season 2 Episode 1

## Agenda

* Housekeeping
* Introduction to Hissoi
  * Initial Spot/Listen checks
  * Yes the Elves are gone
  * No you don't get a copy of the journal
  * No you can't retcon bringing Takintotle
* Cody's character is waylayed by Istus
  * Again
  * And again
* Cody is introduced to party
* Pirates want to pick a fight (slavers)
  * This attracts attention
* Hook???

## Housekeeping

I've already let it slip that I intended to do this, but as we've gotten through level 10 and the need to solidify the image of our character in the face of the onslaught of content that 3.5 provides, I am withdrawing the limited source requirement. The intent of this source requirement was to encourage refinement of your characters and the requests I received to focus existing builds into more representative archetypes of the character you play shows that the training wheels are ready to come off.

I still hate Duskblades.

## Introduction

The party has just been teleported to Hissoi by Daegren who will be accompanying his grandfather home. Tagging along with Alphonse, Cassandra, and Jackshaar are a slate of companions who have likewise been displaced from their home. 

Rog the Orc, an unlikely companion, has come a long way from his robbing days under the guile and influence of Avee. His Ancestral Relic, a Great Club cracked and broken by his father, now rests reforged on his back as a pure manifestation of his resolve.

Shooki'Alui, the now fiance of Alphonse, is still coming to grips with his recent change in faith after being forcibly removed from his worship at the behest of the Whale Mother herself. Expressing a commitment to Ishtishia has restored his powers, but can the connection truly be what it once was with his ancestral patron deity?

The Moth brothers, Behemoth, Mammoth, and Nammoth, come in tow representing the last of the Blood Legion free from the influence of the new Triumvirant. Behemoth leads his brothers, falling in line behind Alphonse who represents his last hope at returning to a home he recognizes. Hopefully more recognizable than his now much transformed brethren.

Finally, Maligard slinks along, disdainful of all around him. The sins of the past month still linger in the air between himself and his would-be saviors. His dark rueful glare almost seems cheerful in the dismal atmosphere which the party has arrived in.

As the Elves take their leave, whisked away in a swirl of Arcane magics, the city of Hissoi sprawls forward from the end of the sparsely populated pier. Gone are the spires and blocks of the planned city Kath. The looping, but spacious caverns of Underkath are long behind. Forwards are narrow streets, grimy unkept cobblestone, and haphazard construction threatening to pitch itself into the populace walking amongst them.

## Surroundings

### The Tower

The city of Hissoi slopes from the inland. The tightly constructed buildings leave little separation between them giving the impression of a wave of shingles broken by the occassional collapsed structure spilling down upon you. At the apex of the terrain, a spire rises above the rest of the city, mundane and ominous in appearance.

The stone masonry is exposed on the tower, though the entire structure seems to shimmer as if a mirage in the distance. Your eyes tell you repeatedly that the great structure is mundane, but something in the pit of your stomach or the back of your mind insists there is something more...

### The Docks

Working the dock is a vast collection of groups, but they seem highly segregated and insular. Little shouting and bellowing occurs, but instead sorted whispers and short barked curses make up most of the verbal communications.

A Goblin, patch-eyed and scarred, cracks his whip at a group of misbred humanoids from undiscernable backgrounds. Wicked eyes burn with a glowing ember from underneath the bushy eyebrows of a dark-haired dwarf looking on his crew of kinsman loading cargo. Some formless ephemeral humanoid, almost devoid of any noticeable feature at all, slinks amongst the groups who shy away from the stranger.

No two groups are alike except in their demeanor hostile to others.

### Streets

Grimy cobblestone slips into muddy pits and puddles, as the mixed footfalls of hard leather and soft feet fall upon them. Within the grout looks to be traces of mold attempting to cling to every passerby as they take a step.

## Encounter

Greer Gunderson & Emory Zikhail

Skulk and Human

Mess of Orc slaves

Greer darts in and out of combat making precision strikes while Emory uses his Martial training and auras to augment what would otherwise be a slew of ineffective minions.

## Aftermath

Greer and Emory are either dead or running, but the eyes of the whole shipyard are now sizing up the new power in town. Targets feel animosity or intent; the predatory atmosphere feels more like you're prey and the only question they seem to be asking is who get the choice meat.

## Things Introduced in the Session

The Aftermath - A Dwarven tavern which is little more than a hole in the wall with a single locked room upstairs. The other rooms do not have keys.

Reynold - An adult Halfling looking to become a Transmuter. He is unlikely to be very successful without a lot of help. So far, he's been able to do little more than wrote memorizing.

Greer and Emory - Orc slavers and damn is Greer a racist POS. Also he got bodied.

Hissoi Blood Legion - Their colors are navy and maroon. Their emblem is a medallion covered in blood. Unbeknownst to the party, the medallion is a copy of the Seal of the True Guardsman indicating that the Blood Legion is opposed to the well being of Kath
