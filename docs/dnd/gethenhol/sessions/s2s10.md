# Season 2 Session 10

This is a recollection of events as they played out on the fly.

## Commitments

These are the established plothooks and characters introduced during the session.

### Dia

Dia has been introduced as Matt's Bard thrall. She's there to "write" a story about them but has some amount of medical training. And is apparently a perv??? She has shown interest in Alphone and Shooki's relationship, but that is mostly due to the erotic nature of underwater sex.

### Meme

The Ravarora impersonator has been unmasked as Meme the Changeling. Meme is after Kssith to reclaim the power of the Prophet for the church of Istus.

### Takintotle Wants to leave

Takintotle has hired the adventurers to get him and his family out of Kath. Payment is freeing the lions. Kinda. He gave his bakery to Kssith as payment for his question.

### Base Building

The party has taken over the remains of a wizard shop in the sewers spanning the space between the old Blood Legion base and the High Garden. A rune can be used to disguise the shop face, the shop and attached wizard house, and be altered as a rune in order to give allowances in seeing through the illusion.

### Unlocking Cal's Soul

Mini-dungeon to unlock his soul. It's largely rune/ritual based. 7 Zones total.

Up to 3 combat encounter.

Cornelius can bypass at least 2 rooms.
