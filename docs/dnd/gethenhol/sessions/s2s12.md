# Season 2 Session 12

## Agenda

* Housekeeping
  * Recap
    * Shopping
    * The Base
    * Beautiful People
    * Colors
    * The Plot
  * Level Check
  * Gear Check
* Blackwake Venture


## Housekeeping

### Recap

#### The Base

Takintotle has taken the kitched. It's his now.

#### Beautiful People

Dia cast a spell from the Book of Erotic Fantasy to make Cassandra and Alphonse beautiful. They were also briefly more charastimatic, but the Appearance change lasted hours greatly exceeding the beauty standards we had come to expect thus far.

Rog was caught quite off-guard by the whole thing and cancelled his plans at the slightest prompting from Cassandra. Alphonse has yet to make sufficient use of his beauty, but after the party comes back together, I'm sure Shooki will give him some ideas.

#### Shopping

We gonna go clothes shopping! Also we brought a minotaur. He's rank but no one seems to mind. Actually, no one seemed to mind anything. The entire ordeal was completely uneventful despite the invasion into the inner sanctum. It's almost curious given the lack of disguises involved.

#### Colors

Everyone gets their pallete swap. Except Dia. She gets the pallete swap we tell her to get. No infringing on the protagonist(s)!!

#### The Plot

Turns out KC is putting up a front for the city and is rather uninvolved. Being a non-political political powerhouse does have the benefit of giving their recruiters free reign. Also they've pulled in some of the former Blood Legion children whom Alphonse had been training.

In addition, Cornelius has inuited upon the nature of the magic being used to manipulate the portable hole containing the sword to which Cal's soul is bound. The magic is designed to bend reality but the reality is confined to the pocket dimension. Whatever happens in the dimension won't have any inherent affect on the world, but the events are manifestations of pivitol events in Cal's life. Altering these events will alter the soul bound to the sword within the pocket dimension.

Cornelius gave a dire warning to the party that events within the pocket dimension could be changed in a way that will produce a different Cal than the one we know today. While this won't alter the events of the past, they could be interpreted in wildly different ways from the perspective of the new person to emerge from the alteration. Beyond that, the blood bond shared by Cal and the Blood Legion has linked their souls to which would cause a ripple effect throughout the organization.

With a stroke of genius verging on an epiphany, the Geometer also devised a method by which the souls of other individuals involved in the scenarios could be affected. This would involve a determination of the participants which itself requires an investigation into Cal's past and laborous examination of the dimension itself. Ultimately, this feat could allow the party to make profound changes on a national scale depending on whom they decide to include amongst those whose lives Cal has touched.

### Level Check

Just checking, the entire party is now level 11, correct?

## Black Wake Venture

With the shopping in the elven distric completed, the "procurement" division of the party made way to the other shopping district only to meet up with the plot drivers in the Ishitishia district. Collectively a plan was hatched to question Kilowag about Cal's past, but her reticence turned it into a simple invitation to Pa's bar for the evening. Despite not being a drinker, something within her made her accept the rather peculiar solicitation.

### Split the 'Party'

Having set the agenda for the evening, the main party made way to the Blackwake camp outside the city in order question KC. Given the limitations of their familiarity with the area, it was determined to take a single teleport trip with Jackshaar leaving Rog, Shooki, and Jamsheed to return to the underground.

Dia, Jackshaar's thrall, was brought along by her master's demand thinly veiled as an insistence that she collect the necessary information for her story first hand. Thus, the PCs went along their way to hopefully not fuck up and die.

### The Camp

Upon reaching the camp, they realized that their goals would be difficult to achieve across the board. A gigantic crater now lay where the camp once was, easily surpassing the original boundary of the compound. On top of that, three purple wurms were patrolling the exterior walls of the crater looking for their next meal.

The party struggled with the wurms, succeeding in killing one in a mighty blast which wrent it assunder, and saved Dia from the mouth of another. And by the party, it should be known that it mostly means Cassandra did all of that while Cornelius ran. Dia was promptly eaten.

Almost as if on cue, a mysterious hooded figure approached and revealed himself to be Alphone 'Thillidi'. With a snap of his fingers, he reassembled the wurm corpse into a new minion for himself in a manner that does not befit typical casting patterns (in fact, he was really just signalling some of his thralls in jars to do his bidding in creating minions for themselves through a constant scrying which had been established on his position).

Jackshaar gave a token attempt to restrain the blue-faced man