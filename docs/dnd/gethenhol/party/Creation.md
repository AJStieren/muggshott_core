# Character Creation

Character Creation for Gethenhol has a handful of restrictions and additions. The following is a summary of those rules:

* No Evil Alignments
* Psionic Warriors are not permitted
* Factotems are not permitted
* Planar classes, races, or variants to the former are not permitted 
* Classes may be drawn from a maximum of 3 sources (subject to approval) including variants
* Spells, feats, purchased equipment, and races may only extend 2 additional sources beyond those used for classes. Related books may be grouped as a single source.
* * PHB, PHB2, DMG, DMG2, MM, and the Miniature's Handbook count a single source
* * Warlocks, Warmages, and Wu Jen count Complete Arcane and Complete Mage as a single source
* * Shifters and Warforged treat Races of Eberron and Ebberron Campaign Setting as a single source
* * Psionic Races count Expanded Psionics Handbook and Complete Psionic as a single source
* * Soulmelds and invokations from Dragon Magic do not count against limited sources for Meldshapers and Invokers respectively
* Tome of Battle classes require three levels of PHB Fighter
* The Arcane Archer progresses in qualifying caster at levels 2, 3, 4, 6, 7, 8, and 10
* The Sharakim LA is ignored unless the Duskblade class is used
* Characters without affiliation with a Good-Aligned deity may not use the Book of Exalted Deeds prior to 4th level
* Characters with crafting feats may use the Craft Points variant found on the SRD starting fomr the level the feat was acquired

# Gameplay Changes

* Two Weapon Fighting Rangers may use a d10 HD
* Archery Rangers may use a d10 HD provided they do not multiclass into Fighter or Scout
* Bard has 8+Int skills as a base line
* Paladins receive a bonux Fighter Feat at levels 4, 7, 11, and 16 counting as a Fighter of half their level for prerequisites
* Assassin Alignment Requirement is now "Any Evil or Any Lawful"
* Prestigeous Paladin does not grant access to Paladin spells to Cleric, Facord Souls, Druids, Spirit Shaman, Ur-Priest, or Apostle of Peace
* Dispel Magic and Dispel Psionics suffer a -5 penalty when affecting the other mechanic
* Spontaneously summoned creatures cannot use Spell-Like Abilities unless summoned by a Sorcerer or Spirit Shaman
* Persistent Spell and Extend Spell cannot be used simultaneously
* Item Familiars do not grant bonus experience
* The Eldritch Knight may ignore 5% spell failure at level 2 and an additional 5% at every other level afterwards
* The Eldritch Knight receives a d8 HD provided Duskblade is not a qualifying class
* Dragon Disciple receives effective caster progression instead of bonus spells
* The Soulknife has a full Base Attack Bonus progressions (+1 at each level)
* Raise Dead increases the subject's age by 5 years and the caster's by 1 year
* Ressurect increases the subject's age by 5 years and the caster's by 2 years
* True Ressurection increases the subject's age by 10 years
* Wish and Miracle age the caster 1 or more years depending on the effect
* Shaped or sculpted Anti-Magic Fields must include the caster at all times
* The Erudite variant of Psion may not benefit from Anarchic Initiate class features
* Summon Monster is subject to the individualed summoning list variant found on the SRD
