# Cal's Domain

## Combat - Slave Orphan

A stone floor rushes upwards to meet you from the depths of the void while the podiums before you simply vanish. Soft torchlight dances across clean stonework showing the initial signs of aging and wear. As the flames dance, you notice the yellow flames seem muted as if drained of their color.

**Skill Check: Knowledge (Nature) or Survival with Darkvision**<br>
15 - The flames look as if they were being viewed through Darkvision, but light sources should not be able to lose their color from darkvision by definition

**Skill Check: Knowledge (Arcane)** <br>
15 - Illusionary flames lose their color in the dark if they are not made to shed light<br>
20 - The representative nature of the scenarios being presented by this domain indicate that everything is in some way illusionary relative to the material plane<br>
25 - Interference within this domain intended to manipulate Cal's soul is only responsible for forcing the scenario manifestation, the manifestation itself is the spiritual representation of what occurred causing elements to be dimmed or altered based on perceptions<br>
30 - A strong will or True Sight can bypass the representative nature of these scenarios

The party finds itself in a large room with many orthoganal niches and doors. Looking at the ceiling, countless arches and beams haphazardly give stability to the unwieldy structure which feels to be strung out as more of a compound. Even within this structure, entire buildings seem to be constructed out in the open disrupting the space.

**Skill Check: Knowledge (Architecture and Engineering)**<br>
15 - The super structure of the room isn't sound on its own, but the beams and archways in the ceiling appear to keep it contained<br>
20 - Interlaced substructures appear to have been created both for their own structural purposes and to shore up the weakness of the larger super room.<br>
25 - Beams, archways, and support walls appear to have overlapping purposes which have made many of them redundant<br>
30 - It dawns on you that the architecture was obviously built across multiple design paradigms with varying levels of engineering practices, but with a unifying goal to establish this compound. The initial roofing structure of beams provided the support to create the subwalls which became independent structures within the compound. Archways were added later as beams were deprecated and replaced to take the load off as many bearing walls as possible. The masonry shows that these improvements were done in cyclical waves by different contruction crews<br>

Denizens of the species which populate Underkath mill about in directed and purposeful patterns. Cliques within the groups seem to be established largely along racial lines, but many seem to be more vocational as well. A grouping of Orcs and Bugbears acting rather brutish and obtuse includes a rather muscular Hobgoblin. Small Goblins might have a lithe 

Childhood Witness

## Combat - Obtaining the Falchion

A stone brick chamber materializes before you, roughly 40 by 70 feet in an irregular shape. Mortar dust wafts through the air carried along by a rank stench decay and sewage. Looking about, you can tell through dim lighting that there are obvious apertures exiting and entering the room only to stare at an eerie darkness through those barriers. Wisps of dark mist seep from what you assume are various corridors.

Size pillars of stone line the great room holding aloft a coffered ceiling, 30 feet between each of them beginning a mere 5 feet from the wall. The middle pillar on the right and left column furthest from you have been shattered leaving their debris about the room.

**Skill Check: Spot**<br>
15 - The flow of the misty darkness does not match the mortar dust indicating that it is not a physical material nor intended match this environment

Disembodied voices murmur about you in undefined chatter, too low to make out yet emanating from your immediate surroundings. A luminescent presence shimmers into existence within the chamber followed by another and another. Four such indistinct entities, each roughly the size of a medium creature, unsubstantially illuminated in a white mist much the opposite of the darkness near the edge of the room.

**Skill Check: Spot**<br>
15 - The flows of misty light displaced the mortar dust and even create airflow as they navigate the room.<br>
20 - A fifth entity hides amongst the debris, this one simply being an immaterial mist devoid of luster.

At the end of the chamber, a fifth presence erupts from the darkness, but contained within the presence is the Falchion at the center of the chamber you had just left. The motion of the entities already occupying the room with you halt suddenly and the mists appear to consolidate and sharpen while the murmuring pauses momentarily. Even at a glance, you can now tell the figures within the mist are some variation of humanoid.

"What are you doing here?! This wasn't the plan!" A panicked young man's voice echoes in the chamber. Mist dissipates from one of the figures revealing a human in formal but tattered attire. His gaunt features bely hunger and rags wrapped around his arms and torso bind unfinished wooden boards as a makeshift armor.

"This isn't the escape route, we're the distraction." An aged woman's voice, not touched by wisdom but merely time, seems to entreat the new entity. She emerges from the mist closest to the bearer of the falchion, opened hands and a look of almost tearful concern from the a face distorted by far more than age. The remains of servant's garb hangs from her emaciated form. 

**Skill Check: Nature**
15 - The woman is a Mongrelfolk

In rapid succession, a Darfellan wearing a comically stereotypical pirate's costume and another with an equally archetypal butler's suit complete with tails and snow white cloth gloves emerge from the other indistinct entities.

"Conjurer, make haste for the correct rendevous with the master"
"Aye, Conjurer. The maid and squire shall be fine in me care."

The scene freezes for a moment, the dust in the air ceases its flow and even the rags around the squire stop swaying. Dark mists swirl and encroach upon the room from the corridors before purple missiles erupt from them to strike the remaining misty figure or dissolve into the terrain around you.

The scene about you stutters between what you see and something that seems to be occurring beneath it before an elven hand materializes around the falchion, as it is menaced towards the servants. A pompous arogant voice oozes forth "As if I were to disappoint The Master. The coming conflict will be a testiment to the wrath of the Blackwake."

The Darfellan pirate opened his mouth in alarm only to have the look of surprised frozen upon his visage for the last time. The pale dark haired elf released the blade only for it to fly of its own volition impaling the sailor, his eyes alight with a malevolent purple glow. Sparks of power run along his arms in a show of power as a figure darts out from the debris to where the pirate's body lay slain, a young hobgoblin trembling as he desparately tried to find some sign of life in the corpse.

"There is my little scapegoat, ready to take the fall."

**Start Combat**

Cal claims the falchion over Buck's control, but Buck summons a horde of monsters to kill the party and interlopers. Upon being defeated the scene resets and plays the following from the point at which the world stuttered.

### Revised Scene

The scene resets with each figure back in its place but fully revealed. The human dressed not in rags, but in well crafted leather armor stands as an armed swordsman. The older maid is no longer a crone of a mongrelfolk in tattered dress, but a mature Dwarf in formfitting leather armor and a discarded dress at her feet. Pirate and Butler are simply sailors, captain and first mate. Finally, the young frail Hobgoblin which was hiding now stands among his comrades looking at the Elf with concern and affection before the scene begins again.

No words are exchanged before the falchion again launches itself impaling the captain before the banter starts, once more oozing with arogance. "As if I were to disappoint Father. The coming conflict amongst the Blackwake will be a testiment to my wrath and endeer me to him."

Cal checked the corpse before pulling the falchion from it, "What are you doing?! We were supposed keep this from becoming violent and shield the master."

The Elf smile malevolently as the Hobgoblin held the blade limply, "There he is. My perfect scapegoat upon which to blame this whole massacre. Make sure to bleed so I can plant the evidence back with the other corpses."

From there the scene played out without you able to interfere. The Conjurer brought forth his minions, not nearly so great in number, and attempted to control the sword once more. He was shocked when Cal claimed a bond with the falchion and shocked further when Cal struck him down.

"Who is the scapegoat now, Buck?"

The Elf smiled cruelly at the Hobgoblin as he coughed blood. "Still you. It will always be you, Goblin."

## Combat 3

Death of the legion

## Puzzle 1

Within the nothingness, a twinkling light shines from above illuminating glittering particles in the air. The light widens along one axis and the shimmering dust parts into two distinct barriers. Warm dry air erupts from the space between before a portal materializes within the light.

Sand pours out of the portal onto the ground before you, coarse and dry to the touch.

Chakra routing

## Puzzle 2

Statue relations

## Social 1

The scene opens in the police processing with the categorization of the slaves rescued from the operation. The children all find out that the adults were killed on the way out which is a bit of propaganda intended to dishearten them and separate their lives from the 

Orphan solidarity

## Social 2

Save the politician
