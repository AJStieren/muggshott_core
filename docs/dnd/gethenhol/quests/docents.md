# Docents

The Docent expeditions are a major driver for multiple plot lines relating to the War Initiates of the Blackwake. KC3's quest for the docents amounts to him trying out different docents in hopes of finding the perfect fit, though rumors have run amok insinuating that it might be looking for a specific docent.

## History

Docents are a product of the progenitor Quori race. The Quori had a split amongst its ranks which was exploited by the denizens of the "origin worlds" which would eventually be split and forced into the cycling planescape. A combination of races, primarily led by the ambitious Humans and manned by the short-lived Goblins and Orcs, conspired against the political schemes of the Quori and leveraged their own 
