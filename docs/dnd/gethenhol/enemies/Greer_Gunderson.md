# Greer Gunderson

*Race*: Skulk | *Gender*: Male

## Base Characteristics

Medium Humanoid

Armor Class ??
Hit Points ? (?d?)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 14  | 10  | 10  | 8  | 6  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

*Stats are unknown as of yet.

## Abilities

### Peerless Camouflage

May move at full speed and run without taking a penalty on Hide checks

### Innate Nondetection

Any attempt to use Divination requires a DC 20 CL to succeed

### Trackless Path

+10 to DC to track

### Racism

+1 vs Orcs and Half-Orcs

## Skills

Skill           | Base
 -------------- | ----
Hide | +15 (racial)
Move Silently | +8 (racial)

## Saves

Fort | Reflect | Will
+0 | +3 | +0

## Backstory

Emory Zikhail is a partner in crime to Greer Gunderson. The boastful human has a keener eye than he lets on, often letting other believe Greer to be the big plotter despite having equal input. He plays heavily on Greer's distaste for Orcs and has used it to Shanghai more than one uncooperative soul into his service.

## Interactions
