# Emory Zihkail

## Base Characteristics

Medium Humanoid

Armor Class ??
Hit Points ? (?d?)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 14  | 10  | 10  | 8  | 6  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

*Stats are unknown as of yet.

## Abilities

## Skills

Skill           | Base
 -------------- | ----


## Saves

Fort | Reflect | Will
+0 | +3 | +0

## Backstory

Greer Gunderson is a partner in crime to Emory Zikhail. He's mischeivous at his most ambivalent and malicious to those who grab his attention.

## Interactions
