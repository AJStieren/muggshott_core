# Degrin Unmade

The body of Degrin Bo has been separate from its mind and head, replaced with a gruesome likeness. Some semblance of will must remain in the body or soul as it rampages through catacombs.

## Base Characteristics

Medium Horror <br>
*Race*: Fiendish Darfellan | *Gender*: Male <br>
*Armor Class*: <br>
*Hit Points*: <br>
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 15  | 10  | 12  | 14  | 16  
(+0)| (+2)| (+0)| (+1)| (+2)| (+3)
