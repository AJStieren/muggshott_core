# Mad Bo

The head of Degrin Bo has had its mind or soul taken to create Degrin Reforged, but the physical memories and learned values of Degrin Bo remain in the still animated head. The head has been mounted into an undead creature stitched together from the bodies of many other humanoids. The Steward of the Storm has been incorporated into the vessel and the divine connection stolen from Dethihilda to give Mad Bo divine properties. Kin has altered the connection to dark purpose aligned with her own master, Tiamat.

## Base Characteristics

Medium Undead <br>
*Race*: Darfellan Blaspheme | *Gender*: Male | *Alignment*: Lawful Evil<br>
*Hit Dice*: 18d12+30 (147 HP)
*Armor Class*: 32 (+10 Natural +2 Deflection +6 Chainmail + 4 Dex) <br>
Speed 40' | Initiative +5 <br><br>

**Base Attack/Grapple**: +9/+18 <br>

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
32  | 18  | --  | 14  | 15  | 8
(+11)| (+4)| (--)| (+2)| (+2)| (-1)

__Saves__ <br>
**Fort**: +6 | **Reflex**: +10 | **Will**: +13

**Special Attacks**: Blasphemous contact, erratic charge<br>
**Special Qualities**: Darkvision 60', DR 5/slash, Immunity to Cold, Inescable Craving, Undead Traits

### __Combat__ <br>

__Nonlethal Strike__ <br>
When making a non-lethal attack against a flat-footed opponent or flanking, the justiciar's attack deals an extra 3d6 damage. Should the justiciar score a critical hit with a nonlethal strike, this extra damage is not multiplied.

**Attack**: +20 Unarmed Strike (1d8+11) <br>
**Bite**: +20 Bite (1d10 + 16 + Blasphemous Contact)

**Blasphemous Contact (Su)**: Each time a blaspheme bites a nonevil creature, the creature is dazed for 1 round and takes 1d6 points of Strength damage. There is no saving throw against this effect <br>
**Erratic Charge (Ex)**: When a blaspheme charges, it can make one turn of up to 90 degrees during its movement. All other restriction on charges still apply.

### Equipment

__Armor__ <br>
+1 Mithral Chainmail of Nimbleness

__Items__ <br>
* Manacles of Silence
* Masterwork Manacles (med) x2
* Masterwork Manacles (small) x3
* Masterwork Manacles (large) x2
* Masterwork Manacles (huge)
* Gloves of Titant's Grip
* Belt of Strength +4
* Amulet of Natural Armor +1
* Ring of Protection +2
* Dimension Stride Boots
* Universal Solvent x2
* Potion of Bear's Endurance
* Blessed Bandages x5
* Tanglepatch x2
* Sunrod x5
* Smokestick x2
* Signal Whistle
* Baldric Belt
* Belt Pouch x7
* Lesser Psionic Restraints x2
* Average Psionic Restraints x2
* Greater Psionic Restraints x2
* Damping Psionic Restraints x2

### __Feats__ <br>
* Quick Draw
* Combat Expertise
* Improved Feint
* Track
* Flick of the Wrist
* Skill Focus(Gather Information)
* Exotic Weapon Proficiency (Manacles)
* Improved Unarmed Strike
* Scorpion's Grasp
* Weapon Focus (Grapple)
* Mage Slayer

