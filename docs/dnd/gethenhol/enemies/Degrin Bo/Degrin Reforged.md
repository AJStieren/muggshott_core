# Degrin Reforged

The mind of Degrin Bo was separated in an experimental procedure and some fracture pieces of it introduced to a construct. Now those pieces want to rend the ones who broke him asunder.

Degrin Bo's mind has been converted into a Battlemind Docent, an adorably small construct about the size of a rat that skitters between your legs, shaped like a beetle with six legs, two pincer-like arms, and a stiff adamantine carapace. This thing seems mostly harmless, until it clambers up the arm of the nearby iron golem and attaches to its chest. Suddenly the iron golem's eyes light up with a terrible piercing intelligence...

## Base Characteristics

[Large Construct](https://dnd-wiki.org/wiki/Battlemind_Docent_(3.5e_Monster) <br>
*Race*: Construct | *Gender*: -- | *Alignment*: Lawful Evil<br>
*Hit Dice*: 18d10+30 (129 (65 shield) HP) <br>
*Armor Class*: 37 (+4 Deflection -1 Size +2 Dex +22 Natural) | *Touch*: 15 | *flat-footed*: 34 <br>
Speed 30' | Initiative +6 <br><br>

**Base Attack/Grapple**: +12/+31 <br>

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
39  | 15  | --  | 14  | 15  | 17
(+19)| (+2)| (--)| (+2)| (+2)| (-1)

__Saves__ <br>
**Fort**: +6 | **Reflex**: +8 | **Will**: +7

**Special Attacks**: Arc Strike, Breath Weapon,  Maneuver-like Abilities <br>
**Special Qualities**: Construct Traits, DR 15/adamantine, Darkvision 60', Fast Healing 6, immunity to magic, low-light vision, shield

## Abilities

**Arc Strike (Su)**: The natural attacks and melee weapons of the construct gain an extra 1d6 fire damage <br><br>

**Breath Weapon (Su)**: 10-foot cube, cloud of poisonous gas lasting 1 round, free action once every 1d4+1 rounds; initial damage 1d4 Con, secondary damage 3d4 Con, Fortitude DC 19 negates. The save DC is Constitution-based. <br><br>

**Shield (Su)**: The construct gains the shield ability, except the shield gained is based on its CR, not its HD. For example a CR 13 creature would have a shield which provides 65 hp. While the shield is active, the construct gains a +4 deflection bonus to AC. User the creature's original CR to determine bonus. <br><br>

**Maneuver-Like Abilities**: The construct gain the Battlemind Docent's maneuver-like abilities, as well as additional maneuver-like at each level from the golem heart discipline. It uses its original CR to determine what level maneuvers it knows and its effective initiator level.

### Maneuvers

**Arcane Consumption Strike (strike)**:

**Body of Stone (boost)**:

**Crafter's Touch (boost)**:

**Dust Devil Blade (boost)**:

**Endure Destruction (counter)**:

**Heavy Body (stance)**:

**Iron Fist (strike)**:

**Mass Impact (counter)**:

**Prison of the Earth Spirit (strike)**:

**Solutio Materia (strike)**:

## Qualities

**Fast Healing (Ex)**: The construct gains fast healing equal to half of its original CR

**Immunity to Magic (Ex)**: An iron golem is immune to any spell or spell-like ability that allows spell resistance. In addition, certain spells and effects function differently agains the creature as noted below <br><br>

A magical attack that deal electricity damage slows an iron golem (as the slow spell) for 3 rounds, with no saving throw.

A magical attack that deals fire damage breaks any slow effect on the golem and heals 1 point of damage for each 3 points of damage the attack would otherwise deal. If the amount of healing would cause the golem to exceed its full normal hit points, it gains any excess as termporary hit points. For example, an iron golem hit by a fireball gains back 6 hit points if the damage total is 18. An iron golem gets no saving throw against fire effects.

An iron golem is affected normally by rust attacks such as that of a rust monster or a rusting grasp spell.
