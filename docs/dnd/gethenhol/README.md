# Gethenhol

Campaign: Gethenhol <br>
DM: Me


## Party

* [Sample](party/README.md)

## Sessions

* [Session Sample](sessions/README.md)
* [Season 2 Session 1](sessions/s2s1.md)
* [Season 2 Session 2](sessions/s2s2.md)
* [Season 2 Session 3](sessions/s2s3.md)
* [Season 2 Session 4](sessions/s2s4.md)
* [Season 2 Session 7](sessions/s2s7.md)
* [Season 2 Session 8](sessions/s2s8.md)
* [Season 2 Session 9](sessions/s2s9.md)

## NPCs

* [Sample](npcs/README.md)
* [Sample+](npcs/README.md)

## Enemies

* [Sample](enemies/README.md)

## Factions

* [Sample](factions/README.md)

## Locations

* [Sample](locations/README.md)

## Quests

* [Sample](quests/README.md)

