Gor is a purebred Orc who owns a bar in Underkath. He's not flamboyant, but very clearly homosexual given with whom he flirts which makes his son Rog a minor curiosity.

When he first came to Kath, he entered into a relationship with Shooki, however the relationship ended due to pressure from the Temple of the Whale Mother and the high priest, Shooki's father. Since then, he's grown old while Shooki has only grown into middle age.

## Influenced

Gor was originally envisioned to have a slight meanstreak with pranks and stunts, but a question from a player led to a much darker (and interesting) interpretation. When faced with the fiance of his former boyfriend, one for whom Shooki had left his family for where he did not for Gor, the Orc conspired to get him drunk and violate him in his stupor. This de-humanizes the character to make Rog more of an anomoly and alienates the party from him to make Underkath uncomfortable.
