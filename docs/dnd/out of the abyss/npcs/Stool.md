# Stool the Porcini

*Race*: Mycanid  | *Gender*: male      | *Eyes*: None
*Height*: 2' ?"   | *Weight*: ???

## Base Characteristics

![Stool](../../../img/dnd/out%20of%20the%20abyss/npcs/Stool.png "Stool the Porcini")

Small Plant <br>
*Race*: Mycanid  | *Gender*: Male <br>
*Armor Class*: 10 <br>
*Hit Points*: 7 (2d6) <br>
Speed 10'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
8   | 10  | 10  | 8   | 11  | 5
(-1)| (+0)| (+0)| (-1)| (+0)| (-3)

## Abilities

### Distress Spores

When the myconid takes damage, all other myconids within 240 feet of it can sense its pain.

### Sun Sickness

While in sunlight, the myconid has disadvantage on ability checks, attack rolls, and saving throws. The myconid dies if it spends more than 1 hour in direct sunlight.

### Rapport Spores (3/day)

A 10-foot radius of spores extends from the myconid. These spores can go around corners and affect only creatures with an Intelligence of 2 or higher that aren’t undead, constructs, or elementals. Affected creatures can communicate telepathically with one another while they are within 30 feet of each other. The effect lasts for 1 hour.

## Home

Neverlake Grove
