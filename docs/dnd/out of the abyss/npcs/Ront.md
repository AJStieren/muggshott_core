# Ront (deceased)

*Race*: Orc | *Gender*: Male

## Base Characteristics

![Ront](../../../img/dnd/out of the abyss/npcs/RONT.png "RONT the dead Orc")

Medium Humanoid Corpse

Armor Class --
Hit Points 0 (0d0)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 10  | 10  | 10  | 10  | 10  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

## Backstory

Ront was part of the tribe seeking to invade Gruntlgrim before being captured with Eldeth on the battlefield. While bound in slavery, Ront was found to be a danger to his fellow prisoners and thrown to the giants spiders on the order of [Ilvara](../enemies/Ilvara.md). He was consumed as a grim warning to his fellow prisoners.

## Home

Probably hell.
