# Derendil

Derendil is Elven royaly who has been cursed to the form of a Quaggoth. He wishes to return to his home of Nelrindenvane and reclaim his heritage.

## Base Characteristics

![Derendil](../../../img/dnd/out%20of%20the%20abyss/npcs/Derendil.png "Derendil the Cursed Prince")

Medium Humanoid <br>
*Race:* Quaggoth (formely Sun Elf)  | *Gender:* Male

## Backstory

Prince Derendil of Nelrindenvane was a high elf turned into a Quaggoth by the evil wizard Terrastor to usurp the leadership of the city.

No one has ever heard of Nelrindenvane.

## Interactions

Was on watch with Takintotle in Session 3 during the early morning shift. Was able to name all of the high elven pastries and taught Takintotle how to make Elven beignets.

## Home

City of Nelrindenvane
