# Shuushar the Enlightened

Shuushar is a kua-toa who has reached enlightenment and embraced a nonviolent philosophy. His greatest ambition is to share and spread his enlightenment amongs his people.

## Base Characteristics

![Shuushar](../../../img/dnd/out%20of%20the%20abyss/npcs/Shuushar.png "Shuushar")

Medium Humanoid <br>
*Race*: Kua-Toa | *Gender*: Male
