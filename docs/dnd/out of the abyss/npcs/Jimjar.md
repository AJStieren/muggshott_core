# Jimjar the Feckless Rogue

Gives no fecks. Loves to make bets for gold. Never forgets a wager, always pays his debts and demands others do the same.

## Base Characteristics

![Jimjar](../../../img/dnd/out of the abyss/npcs/Jimjar.png "Jimjar the Feckless Rogue")

Small Humanoid <br>
*Race*: Svirfneblin (Deep Gnome) | *Gender*: Male <br>
*Armor Class*: 12 <br>
*Hit Points*: 27 (6d8) <br>
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 15  | 10  | 12  | 14  | 16  
(+0)| (+2)| (+0)| (+1)| (+2)| (+3)

## Abilities

### Sneak Attack (1/Turn)

The spy deals an extra 7 (2d6) damage when it hits a target with a weapon attack and has advantage on the attack roll, or when the target is within 5 ft. of an ally of the spy that isn't incapacitated and the spy doesn't have disadvantage on the attack roll.

### Stone Camouflage

The gnome has advantage on Dexterity (Stealth) 
checks made to hide in rocky terrain.

### Gnome Cunning

The gnome has advantage on Intelligence, Wisdom, and Charisma saving throws against magic.

### Innate Spellcasting

The gnome's innate spellcasting ability is Intelligence (spell save DC 11). It can innately cast the following spells, requiring no material components:

* At will: nondetection (self only)
* 1/day each: Blindness/Deafness, Blue, and Disguise Self

### Cunning Action

Jimjar can hide, disengage, or dash as a bonus action.

## Actions

### Multiattack

The spy makes two melee attacks.

### Shortsword

*Melee Weapon Attack* +4, Reach 5 ft., one target
*Hit*: (1d6 + 2) piercing damage

### Hand Crosssbow

*Ranged Weapon Attack* +4, Range 30/120 ft., one target
*Hit* (1d6 + 2) piercing damage

### Weak ass punch

*Melee Weapon Attack* +2
*Hit* 1 (1) bludgeon damage

## Skills

Skill           | Base
 -------------- | ----
Deception       | +5
Insight         | +4
Investigation   | +5
Perception      | +6
Persuasion      | +5
Sleight of Hand | +4
Stealth         | +4

## Backstory

What is your story?

## Interactions

What have you done with [Takintotle](../party/Takintotle.md)?

## Home

Blingdestone
