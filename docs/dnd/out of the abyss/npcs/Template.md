# Character Name

*Race*: | *Gender*:

## Base Characteristics

Size Creature

Armor Class
Hit Points 1 (1d4)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 10  | 10  | 10  | 10  | 10  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

## Abilities

### Sample Ability

Description of the ability, but not necessary the statistics.

## Backstory

What is your story?

## Interactions

What have you done with [Takintotle](../party/Takintotle.md)?

## Home

Where are you from? Where are you going?

