# Eldeth


## Base Characteristics

![Eldeth](../../../img/dnd/out%20of%20the%20abyss/npcs/Eldeth.png "Eldeth")

Medium Humanoid <br>
*Race*: Dwarf | *Gender*: Female  

## Quest

Wants hammer and shield to get back to her father if she dies.

## Background

Prefers Bow and Arrow to Warhammer.

Comes from a family of smiths. Each male in the family runs the smith and gives their first warhammer to a great warrior. Her father gave her his hammer.

## Family

Younger brother - Brotter

### Brotter

Training to be a smith under his father. Made his 

## Home

Gruntlgrim

No one wants to go there
