# Jorlan

*Race*: Drow | *Gender*: Male

## Base Characteristics

Medium Humanoid

Armor Class ??
Hit Points ? (?d?)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 10  | 10  | 10  | 10  | 10  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

*Stats are unknown as of yet.

## Backstory

Jorlan is the former paramour of [Ilvara](./Ilvara.md) losing his position, both professionally and socially, to [Shoor](./Shoor.md). His shame has lead to ridicule from his former subordinates and has begun manifesting as resentment.

## Interactions

Jorlan asked [Vanereth](../party/Vanereth.md) if he would attempt an escape given the chance. Although he could rather care less about the prisoners, Jorlan left the doors open to permit their escape for the purpose of embarassing his tormentors. Even when [Ilvara](./Ilvara.md) caught the party in the wilderness, Jorlan chose to hinder [Shoor](./Shoor.md) rather than make any real attempt at aid. His aim appears to be either winning his way back into [Ilvara](./Ilvara.md)'s good graces or ensuring that they all wind up as failures in their own right.
