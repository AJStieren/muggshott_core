# Ilvara

*Race*: Drow | *Gender*: Female

## Base Characteristics

Medium Humanoid

Armor Class ??
Hit Points ?? (?d?)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 10  | 10  | 10  | 10  | 10  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

*Stats are unknown as of yet.

## Abilities

### Domination

Not an ability, just a personality trait. She is in control. She knows it. You know it. Just get on your knees and hope she has an ending other than death or torture picked out.

## Backstory

Ilvara is the leader of the slave camp imprisoning the party. In the past, she held [Jorlan](./Jorlan.md) as her immediate subordinate and lover until the now jilted former lover was scarred in the line of duty. Lately she has taken to [Shoor](./Shoor.md) as her lover. He is also her most immediate servant, but that means little in terms of competence or performance (out of bed).

Many of her remarks and threats are sexual in nature.

## Interactions

She was initially the target of [Takintotle](../party/Takintotle.md)'s sticky fingers, but caught him in the act. While he begged and groveled for forgiveness, [Jimjar](../npcs/Jimjar.md) was signalled to finish the deed through [Stool](../npcs/Stool.md)'s Rapport Spores.

[Takintotle](../party/Takintotle.md) later had the (dis?)pleasure of observing her intimate moments with [Shoor](./Shoor.md). She was being robbed yet again in those intimate moments, though not without unwittingly inflicting mentals scars of her own to those present.

Ilvara is now in pursuit of the party attempting to recapture them for the slave market.

## Home

????
