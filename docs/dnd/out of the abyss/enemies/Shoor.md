# Character Name

*Race*: Drow | *Gender*: Male

## Base Characteristics

Medium Humanoid

Armor Class ??
Hit Points ? (?d?)
Speed 30'

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 10  | 10  | 10  | 10  | 10  
(+0)| (+0)| (+0)| (+0)| (+0)| (+0)

*Stats are unknown as of yet.

## Backstory

Jorlan is the current paramour of [Ilvara](./Ilvara.md) and otherwise a useless tool. He has been elevated to his position based on her infatuation with him rather than any seeming amount of talent. He's been granted the use of a magic wand, but even that has been unable to compensate for his incompetence.

Despite this, Jorlan seems to hold an implicit superiority to [Jorlan](./Jorlan.md) and revels in mocking him.

## Interactions

[Takintotle](../party/Takintotle.md) later had the (dis?)pleasure of observing his intimate moments with [Ilvara](./Ilvara.md). She was being robbed yet again in those intimate moments, though not without unwittingly inflicting mentals scars of her own to those present.

When [Ilvara](./Ilvara.md) caught the party in the wilderness, [Jorlan](./Jorlan.md) chose to hinder Shoor rather than make any real attempt at aid. [Jorlan](./Jorlan.md)'s aim appears to be either winning his way back into [Ilvara](./Ilvara.md)'s good graces or ensuring that they all wind up as failures.

