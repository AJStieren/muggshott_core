# Cooking

## Course 1

Waterorb, squeezed free of the water it holds, is rehydrated with mushroom wine and seared to retain the fluid before it is slowly roasted. Rippebark is cut into chunks and seared alongside the wine impregnated Waterorb. The roasted Waterorb slowly sweats an oil-like substance flavored with the wine

Zurhkwood flour is combined with the slightly more viscous water from the Waterorb and kneeded into a rough dough. Adding slightly more fluid to the rough dough, it is further kneeded into a smooth consistency and rolled flat. The flat dough is then folded with Bluecap flour giving a marbled blue and gray appearance to the dough before it is rolled as flat as possible once more. The flat dough is the loosely folded, cut into long strips, and the strips are then boiled giving a mixed color set of noodles.

Using the multitude of mushrooms available, Takintotle abuses the variety to 
