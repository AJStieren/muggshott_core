# Dessa

Player: Larissa

![Dessa](../../../img/dnd/out%20of%20the%20abyss/party/larissa.png "Dessa the Storyteller")

**Race**: Satyr | **Gender**: Female
**Class**: Bard | **Level**: 3 <br>
**Subclass**: 

## Abilities

### Mirthful Leap
*Racial*: Satyr

Roll a D8 when making a long or high jump and add that number as feet that you cover. This costs normal movement.
