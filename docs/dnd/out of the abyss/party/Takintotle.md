# Takintotle

**Race**: Kobold        | **Gender**: male          | **Eyes**: Blue <br>
**Height**: 2' 4"       | **Weight**: 35 lbs        | **Background**: [Cook](https://www.dandwiki.com/wiki/Cook_(5e_Background)) <br>
**Class**: [Rogue](roll20.net/compendium/dnd5e/Rogue)        | **Level**: 5 <br>
**Subclass**: Soulknife

## Base Characteristics

![Takintotle](../../../img/dnd/out%20of%20the%20abyss/party/takintotle_close_up.png "Takintotle the Formerly Bad Baker")

**HP**: 38
**Hit Dice**: 5d8

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
12  | 18  | 15  | 12  | 14  | 16
(+1)| (+4)| (+2)| (+1)| (+2)| (+3)

## Progression

Level | Class | Subclass  | Sneak Attack | Psionic Dice | Features
----- | ----- | --------- | ------------ | ------------ | --------
1     | Rogue | -         | 1d6          | -            | Thieves Cant, Sneak Attack, Expertise, Lush, Chef, Skill Expert, Grovel, Cower, And Beg, Dark Vision, Through Their Stomachs
2     | Rogue | -         | 1d6          | -            | Cunning Action
3     | Rogue | Soulknife | 2d6          | 4d6          | Psionic Power, Psychic Blades
4     | Rogue | Soulknife | 2d6          | 4d6          | ~~Ability Score Improvement~~ Mobile (feat)
5     | Rogue | Soulknife | 3d6          | 6d8          | Uncanny Dodge
6     | Rogue | Soulknife | 3d6          | 6d8          | Expertise
7     | Rogue | Soulknife | 4d6          | 6d8          | Evasion
8     | Rogue | Soulknife | 4d6          | 6d8          | Ability Score Improvement
9     | Rogue | Soulknife | 5d6          | 8d8          | Soul Blades
10    | Rogue | Soulknife | 5d6          | 8d8          | Ability Score Improvement
11    | Rogue | Soulknife | 6d6          | 8d10         | Reliable Talent
12    | Rogue | Soulknife | 6d6          | 8d10         | Ability Score Improvement
13    | Rogue | Soulknife | 7d6          | 8d10         | Psychic Veil

## Abilities

### Racial

#### Grovel, Cower, And Beg

As an action on your turn, you can cower pathetically to distract nearby foes. Until the end of your next turn, your allies gain advantage on attack rolls against enemies within 10 feet of you that can see you. Once you use this trait, you can't use it again until you finish a short or long rest.

### Rogue

#### Expertise

At 1st level, choose two of your skill Proficiencies, or one of your skill Proficiencies and your proficiency with Thieves' Tools. Your Proficiency Bonus is doubled for any ability check you make that uses either of the chosen Proficiencies.

At 6th level, you can choose two more of your Proficiencies (in Skills or with thieves' tools) to gain this benefit.

#### Thieves Cant

During your rogue Training you learned Thieves' Cant, a Secret mix of dialect, jargon, and code that allows you to hide messages in seemingly normal conversation. Only another creature that knows Thieves' Cant understands such messages. It takes four times longer to convey such a Message than it does to speak the same idea plainly.

In addition, you understand a set of Secret signs and symbols used to convey short, simple messages, such as whether an area is dangerous or the territory of a thieves' guild, whether loot is nearby, or whether the people in an area are easy marks or will provide a Safe House for thieves on the run.

#### Sneak Attack

Beginning at 1st level, youf know how to strike subtly and exploit a foe's distraction. Once per turn, you can deal an extra 1d6 damage to one creature you hit with an Attack if you have advantage on the Attack roll. The Attack must use a Finesse or a ranged weapon.

You don't need advantage on the Attack roll if another enemy of the target is within 5 feet of it, that enemy isn't Incapacitated, and you don't have disadvantage on the Attack roll.

The amount of the extra damage increases as you gain levels in this class, as shown in the Sneak Attack column of the Rogue table.

#### Uncanny Dodge

Starting at 5th Level, when an attacker that you can see hits you with an Attack, you can use your Reaction to halve the attack's damage against you.

#### Evasion

Beginning at 7th level, you can nimbly dodge out of the way of certain area Effects, such as a red dragon's fiery breath or an Ice Storm spell. When you are subjected to an Effect that allows you to make a Dexterity saving throw to take only half damage, you instead take no damage if you succeed on the saving throw, and only half damage if you fail.

#### Reliable Talent

By 11th level, you have refined your chosen Skills until they approach perfection. Whenever you make an ability check that lets you add your Proficiency Bonus, you can treat a d20 roll of 9 or lower as a 10.

### Soulknife

#### Psionic Power

Starting at 3rd level, you harbor a wellspring of psionic energy within yourself. This energy is represented by your Psionic Energy dice, which are each a d6. You have a number of these dice equal to twice your proficiency bonus, and they fuel various psionic powers you have, which are detailed below.

Some of your powers expend the Psionic Energy die they use, as specified in a power's description, and you can't use a power if it requires you to use a die when your dice are all expended. You regain all your expended Psionic Energy dice when you finish a long rest. In addition, as a bonus action, you can regain one expended Psionic Energy die, but you can't do so again until you finish a short or long rest.

When you reach certain levels in this class, the size of your Psionic Energy dice increases: at 5th level (d8), 11th level (d10), and 17th level (d12).

The powers below use your Psionic Energy dice.

##### Psi-Bolstered Knack

When your nonpsionic training fails you, your psionic power can help: if you fail an ability check using a skill or tool with which you have proficiency, you can roll one Psionic Energy die and add the number rolled to the check, potentially turning failure into success. You expend the die only if the roll succeeds.

##### Psychic Whispers

You can establish telepathic communication between yourself and others-perfect for quiet infiltration. As an action, choose one or more creatures you can see, up to a number of creatures equal to your proficiency bonus, and then roll one Psionic Energy die. For a number of hours equal to the number rolled, the chosen creatures can speak telepathically with you, and you can speak telepathically with them. To send or receive a message (no action required), you and the other creature must be within 1 mile of each other. A creature can't use this telepathy if it can't speak any languages, and a creature can end the telepathic connection at any time (no action required). You and the creature don't need to speak a common language to understand each other.

The first time you use this power after each long rest, you don't expend the Psionic Energy die. All other times you use the power, you expend the die.

##### Soul Blades

Starting at 9th level, your Psychic Blades are now an expression of your psi-suffused soul, giving you these powers that use your Psionic Energy dice:

###### Homing Strikes

If you make an attack roll with your Psychic Blades and miss the target, you can roll one Psionic Energy die and add the number rolled to the attack roll. If this causes the attack to hit, you expend the Psionic Energy die.

###### Psychic Teleportation

As a bonus action, you manifest one of your Psychic Blades, expend one Psionic Energy die and roll it, and throw the blade at an unoccupied space you can see, up to a number of feet away equal to 10 times the number rolled. You then teleport to that space, and the blade vanishes.

##### Psychic Veil

At 13th level, You can weave a veil of psychic static to mask yourself. As an action, you can magically become invisible, along with anything you are wearing or carrying, for 1 hour or until you dismiss this effect (no action required). This invisibility ends early immediately after you deal damage to a creature or you force a creature to make a saving throw.

Once you use this feature, you can't do so again until you finish a long rest, unless you expend a Psionic Energy die to use this feature again.

#### Psychic Blades

Also at 3rd level, You can manifest your psionic power as shimmering blades of psychic energy. Whenever you take the Attack action, you can manifest a psychic blade from your free hand and make the attack with that blade. This magic blade is a simple melee weapon with the finesse and thrown properties. It has a normal range of 60 feet and no long range, and on a hit, it deals psychic damage equal to 1d6 plus the ability modifier you used for the attack roll. The blade vanishes immediately after it hits or misses its target, and it leaves no mark on its target if it deals damage.

After you attack with the blade, you can make a melee or ranged weapon attack with a second psychic blade as a bonus action on the same turn, provided your other hand is free to create it. The damage die of this bonus attack is 1d4, instead of 1d6.

### [Feats](roll20.net/compendium/dnd5e/Feats)

A feat represents a Talent or an area of Expertise that gives a character Special capabilities. It embodies Training, experience, and Abilities beyond what a class provides.

At certain levels, your class gives you the Ability Score Improvement feature. Using the optional feats rule, you can forgo taking that feature to take a feat of your choice instead. You can take each feat only once, unless the feat’s description says otherwise.

You must meet any prerequisite specified in a feat to take that feat. If you ever lose a feat’s prerequisite, you can’t use that feat until you regain the prerequisite. For example, the Grappler feat requires you to have a Strength of 13 or higher. If your Strength is reduced below 13 somehow—perhaps by a withering curse—you can’t benefit from the Grappler feat until your Strength is restored.

#### Skill Expert

You have honed your proficiency with particular skills, granting you the following benefits: <br>

* Increase one ability score of your choice by 1, to a maximum of 20.
* You gain proficiency in one skill of your choice.
* Choose one skill in which you have proficiency. You gain expertise with that skill, which means your proficiency bonus is doubled for any ability check you make with it. The skill you choose must be one that isn't already benefiting from a feature, such as Expertise, that doubles your proficiency bonus.

#### Lush (Flaw)

You have disadvantage on Constitution checks related to the consumption of alcohol or other recreational intoxicants. In addition, you have a reaction after such an occurrence which can be expressed as an emotional outburst, anger, or fainting.

#### Chef

Time spent mastering the culinary arts has paid off, granting you the following benefits: <br>

* Increase your Constitution or Wisdom by 1, to a maximum of 20.
* You gain proficiency with cook's utensils if you don't already have it.
* As part of a short rest, you can cook special food, provided you have ingredients and cook's utensils on hand. You can prepare enough of this food for a number of creatures equal to 4 + your proficiency bonus. At the end of the short rest, any creature who eats the food and spends one or more Hit Dice to regain hit points regains an extra 1d8 hit points.
* With one hour of work or when you finish a long rest, you can cook a number of treats equal to your proficiency bonus. These special treats last 8 hours after being made. A creature can use a bonus action to eat one of those treats to gain temporary hit points equal to your proficiency bonus.

#### Mobile

You are exceptionally speedy and agile. You gain the following benefits: <br>

* Your speed increases by 10 feet.
* When you use the Dash action, difficult terrain doesn't cost you extra movement on that turn.
* When you make a melee attack against a creature, you don't provoke opportunity attacks from that creature for the rest of the turn, whether you hit or not.

## Background [Cook](https://www.dandwiki.com/wiki/Cook_(5e_Background))

You once were a chef, the king of food. Either you were an artisan chef with a fine sense of taste, a master chef who knows dozens on dozens of recipes by heart, or a cook at the mess hall, serving food quickly and on the double. You understand that food has a voice; it can express moods with the right combinations of ingredients, or bring people together with a peaceful meal. What brought you out of the kitchen? A rare ingredient? Maybe your upper chef decided it was time for you to go? Or, maybe, you wanted to go on an adventure to diversify you recipe book? Regardless on why you decided to change pace from your culinary solitude, your cooking and food handling skills will prove vital to an adventuring party. You may even find food where most people don't!

### Skill Proficiencies

[Insight](roll20.net/compendium/dnd5e/Insight), [Performance](roll20.net/compendium/dnd5e/Performance)

### Tool Proficiencies

[Cook's Utensils](roll20.net/compendium/dnd5e/Proficiencies:Cook's\ Utensils)

### Through Their Stomachs

You are able to earn a comfortable living for free during your downtime, by working as a cook. In addition, you are able to feed your adventuring companions modest meals each day for free, unless the DM discerns that you are in a barren place devoid of anything edible for miles, such as a desert or dungeon. You are also able to use your talents to arrange meetings with anyone interested in a free meal. By offering to a cook a free meal for someone who would appreciate it, you are able to have audience with them over that meal, or they may otherwise simply owe you a small favor.

## Story

Takintotle was raised by his maternal grandparents after his father abandoned them for a crazy cult and his mother was pressed into indentured servitude.

He lived in a poor home with few friends but otherwise had an ordinary childhood until he was falsely accused of a crime. A city official recognized the malfeasance and set him free, but he was utterly done with Hissoi at that point.

He tried to leave Hissoi for Kath, but had to smuggle himself onto ships. Failed a bunch but a cook on one of the ships saw a young Kobold failing and decided to teach him some tricks in exchange for capers resulting in actual crime.

Takintotle was decent at the criminal side, but shit at cooking. Rather than pursuing his talents, he dedicated himself to learning to cook and begged for an apprenticeship with more accomplished chefs. His friend at the time took offense and referred the Thieves' Guild to recruit him which involved more crime.

After a few years of leading a double life, Takintotle finally smuggled himself to Kath and was allowed to clean in an Elven kitchen where he actually learned to bake by directly imitating the patissier. To give his life a veneer of decency, he stayed with a single Kobold mother and adopted her children to raise as a family.

The Kath Thieves' Guild was (mis)informed about Takintotle's abilities by his friend when he was spotted selling goods underground. They aggressively tried to recruit him so he stopped going to Underkath and leveraged his family's worth to get them out of the slums. Specifically leveraged through a Lizardfolk broker associated with a church based out of Hissoi. The broker gave an illogical amount of money and good loan terms to Takintotle as though he had been told to.

He side-hustled his own baked goods underground and was found out. After being scolded... a lot by the head chef, the patissier called shenanigans and made him a baker in the kitchen.
That's still a bit shy of where the campaign started, but I figured far enough.

Takintotle is leveraged up to his eyeballs in debt. Which isn't that high given he's a Kobold, but he can't afford the good ingredients and cookware for specialty stuff he learned in apprenticeship. The bakery is very quaint on the whole.

### Family

Maternal Grandparents

#### Grandmother - Igri

Living a humble life

#### Grandfather - Dakli

Living a humble life

#### Father - Szobrek Gimehide

Abandoned the family for a crazy cult. Has a minor gambling problem which is exacerbated by other poor life choices making it more prominent than it actually is. He has fallen into service of Tiamat, a specific sect seeking the power of the prophet.

#### Mother - Gappe Dustgazer

After the birth of Takintotle, Gappe sold herself into indentured servitude under pressure to settle Szobrek's gambling debts lest they result in harm to her family. She has been taken to the mainland in this service by a family so wealthy they were able to buy anonymity with the purchase leaving the family without a means to contact or find her.

#### Brother Oldest - Snukdi

#### Brother Middle - Kobe
