# Session 17

I think it's session 17

This is a post-mortem the morning after while in the middle of a move. 

## Maddening Whispers

The party had found a place within the caverns from which the echoes of the entire city could be heard. Takintotle attempted to learn what Themberchaud most loved to eat. In the process, he was exposed to the carnal escapades of Ilvara and Shoor in the worst of ways scarring him mentally (Madness score up). Eventually he learned that the favorite food was surface fish.

## The Body

The party reeled from the "sudden" death of Droki. An autopsy revealed literally nothing about his death except for the drag marks from him being brought here. Takintotle also took great care to not reveal the bag of holding he held the corpse in leaving the party at an information deficit.

The following was looted from they body:

* 1 gp
* 10 sp
* Spell Scroll of See Invisibility
* 2 Potions of Healing
* A collection of dead bugs
* Makeshift lizardskin book
* Mithral medallion
* Scroll in a copper tube
* 4 small pouches of toenals and skinflakes with labels
    * Dorhun
    * ????
    * ????
    * ????
* Boots of Speed

Takintotle brought the body back wearing the boots he looted from the corpse as the speed boost provided by them was valuable to him. Upon questioning, Takintotle cratered a bluff to withold what he took from the body and gave much of the loot up.

The baker kept the currency, the potions, the medallion, and the lizardskin book to himself. While he hasn't had the opportunity to read the book, it is likely to be in a language he does not recognize as the scroll in the copper tube was.

## Post-debrief

After being informed of the hideout of the Gray Ghosts with whom Droki was dealing, the party was led by Takintotle to the barred stone door originally used for entry. After determining the futility of arguing with a barred stone door, the party began circumnavigating the corridors for another entrance.

Coming upon some dense webbing, the party found giant spiders had nested deeper in the Whorlstone Caverns. Burning away the nest revealed an entrance to the mushroom farms. Takintotle, Adira, and Jun all exhibited unnatural stealth (3 nat 20's in a row) entering the room undetected. They founds themselves at the end of a wide trench serving as a mushroom farm being tended to by the Ghosts as a way to sustain the outpost.

## The Gray Ghosts

The three ghosts present at the time of the incursion were brutally murdered. Takintotle and Jun silently killed two on their own while the third caught sight of the bodies only to die within seconds after.

An alchemist in the backroom was captured and transparently lied about the outpost just being some mushroom farmers. A little intimidation from Adira loosened his tongue and he largely spilled the beans on the operation. Droki was trading for a methodology to spread the madness among the Stone Giants and provided a piece of corrupted metal to the alchemist in exchange for the assistance. The metal was identified by the party as being associated with the Demagorgon.

The alchemist promptly went mad and began babbling incoherently. It was determined that the corrupted metal was as fault and it was quarantined in the mushroom pit away from the party as they made use of the now vacant beds.