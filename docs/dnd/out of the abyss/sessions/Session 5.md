# Session 5

Start of the session... there are fish people. Kua tou?

## Initial Fight

8 normal fish people and 1 special came out of the ocean. Takintotle rang the alarm... by banging on his pot that he was making breakfast in.

Dessa promptly threw her weapon away as she inspired Jun.

Buppido dropped like a rock and was immediately knocked out, but it appeared that the assailants were fighting to capture rather than kill.

Sarith was netted and they started to drag him under the water. Meanwhile, the party was... slaughtering the kua-toa. There were regular references to fish soup.


### Loot

8 Spears
7 Nets
8 Shields (they be gross)
9 Kua-toa garments

### Initiative

Character   | Initiative
----------- | ----------
[Dessa](../party/Dessa.md)       | 22
[Eldeth](../npcs/Eldeth.md)      | -
[Vanereth](../party/Vanereth.md)    | 14
[Takintotle](../party/Takintotle.md)  | 14
[Vaako](../party/Vaako.md)       | 13
Fish person | 11
[Jun no Suke](../party/Jun no Suke.md) | 9
Fish People | 7

## Ploopploopeen

An hour after the fight, [Ploopploopeen](../npcs/Ploopploopeen.md) showed up which made the fish soup made from the slain Kua-toa an awkward sight. However, he was totally fine with this semi-cannaballistic act and called the slain heretics.

The party was quickly convinced to be bait for a monster to solve his problem for half of the existing offerings to Blipdoolvpoolp. Takintotle abstained and quietly sipped his soup.

So... the party hopped in his boats and agreed to be offered as a peace offering to the followers of the Deep Father.

## Bloppblippodd

The party accompanies the Archpriest to his home. The city is populated by incredibly productive Kua-toa. Breaking the productivity and near silence, Dessa and Vaako play [music](https://www.youtube.com/watch?v=QIvzPoA-Vq0) throughout the city. Ploopploopeen audibly questioned the validity of his plan with such an eccentric bunch of misfits.

Eventually... the eccentric fuckwits instigated an incident which caused Vaako to take a beating. Takintotle gave up immediately. Dess and Vanareth eventually did the same.

Takintotle used his Psychic Whispers to establish a link with Ploopploopeen and the skiff person as they left. Possibly due to inhaling too many mushroom spores, he's become somewhat telepathic.

### The Sacrifice

A female archpriest of the Deep Father at the alter was told that the other Kua-toa were capitulating to the cult and the party was being offered as sacrifice. Hemeth, the Dueregar, was brought up to the alter as the initial sacrifice. He was saved when the Archpriests began fighting which spilled out into the ocean. As the fighting grew more feverish, the party ran while making a few strategic strikes.

Meanwhile, creatures had gradually risen from the deep to feast upon the bodies of Kua-toa being slain in their quite uncivil war. In the heart of the fighting, the Archpriestess of the Deep Father was slain. As if triggered by the violence, a great demon rose from the water.
