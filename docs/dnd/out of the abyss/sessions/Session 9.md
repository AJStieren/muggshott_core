# Session 9

Into the Spiderverse.

The party has found the Silken Paths, a shifting path of spiderwebs which can be traversed, and been approached by Yukyuk and Spiderbait who offer to  lead the party through for a total of 4 GP per day. Jun no Suke was carried by Vanareth in order to deal with the

Start of Spider Trail gold: 50 gp per person

Vanareth pays Yukyuk and Spiderbait

## The Spiderverse

While traversing the Silken Roads, Yukyuk returned to the party to tell them there was a being talking about demons up ahead. To be specific, it said "beware the demons in the dark" and "demons rising in the dark". Eventually it appeared to be a Spectator mumbling and watching.

During the eerie observations, Takintotle made the following bets with Jimjar:

* One of Bruce, Spiderbait, or Yukyuk would take a figurative dirtnap before leaving the Silken Roads
* The Spectator would be attacked

The Spectator screamed out "you shall not bind me again!" and attacked the party.

### To ARMS!!

Eldeth was the first victim and struck early with a Ray of Confusion after which she shot Hemeth. Then Dessa shattered the web and dropped the entire party almost 100 feet to more web. Jimjar finished off the Spectator and Eldeth was entangled until recovering from the confusions

#### Loot

4 Pieces of Crimson Coral
Silver Necklace with Gemstone Pendant
Oil of Slipperiness

### Camping

Bupido helped Takintotle cook dinner and actively showed him how to bake fungus into the bread. Takintotle totally failed to decipher it.

Dessa asked to help Takintotle cook, but Takintotle asked her to go get water. She went with Yukyuk and Spiderbait succeeding in finding 12 gallons of water. Jimjar challenged the validity of Takintotle claiming that the Spectator was attacked by him when he shot under the premise that self-defense rendered it a defensive maneuver. He attempted to have Derendil back him up on the matter, but needing Takintotle to translate let Takintotle easily deceive him as to how Derendil came down on the issue. Afterward, Takintotle established a psychic connection with Bupido and Derendil while Jimjar when to speak with Hemeth.

During dinner, Hemeth was victim to the Sparkbread concocted by Bupido. Fed up with the whole party for the day, he chose to sleep apart from them and rolled out bedding on the webs.

**Watches**

* Vaako and Dessa
* Jun no Suke and Jimjar
* Sarith and Stool
* Takintotle and Derendil

#### Watch 1 - Vaako and Dessa

It was uneventful. They talked a bit and something about evil shenanigans. Dessa pulled out weaver's tools and the two established an intent to create something out of the spider threads.

#### Watch 2 - Jun no Suke and Jimjar

Jimjar attempted to goad Jun no Suke into attempting the flip Spiderbait and Yukyuk do with a 5 gp bet.

Attempt 1: Successful flip, poor landing (face)
Attempt 2: Sick flip, unfortunate landing (face first into a mushroom)

After getting his head stuck in a mushroom, Jun exercised his strength to launch himself from the mushroom and finish the somersalt winning the bet.

#### Watch 3 - Sarith and Stool

Sarith got high and missed Hemeth falling to his death.

#### Watch 4 - Takintotle and Derendil

Takintotle immediately checked on Hemeth. He was gone. Takintotle confronted Sarith on the issue and it appears as though Hemeth has fallen to his death due to the webs on which he was sleeping catching fire. Vaako woke up and noticed the bread crumbs near the edge of the plateau leading Takintotle to believe it might be Bupido escalating from pranks.

#### After Watch

Pulling up the body

Scale Mail
2 +1 Handaxe
Journal - He was an arms dealer

Journal Entry:
Going to Slupdiblup was profitable for him as he armed both sides of the civil war. When captured they took:

* Electrobind Trident
* Multiple High Quality Tridents
* Multiple Whips
* Crossbows
* 5 Quivers (top 2 bolts of high quality, remaining lower quality)

Final Entry: He had traversed the Silken Paths before. He was lonely due to the lack of attention.
