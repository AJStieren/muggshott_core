# Session 8

Aftermath of the fights and loss of [Shuushar](../npcs/Shuushar.md), the party moved on and got shithoused in some aquatic fights. [Takintotle](../party/Takintotle.md) debuted his psychic bread knives to the party during one of the ventures and one of the Kua-toa boat drivers were eaten.

^note: Blaise is super tired due to childcare^

## The Night

### Watch 1

Jun, Vanko, and Jimjar

Jimjar noticed the Kua-toa working on the boat and challenged the others on watch to a 10 gold wager that the maintenance would be done within an hour.The remaining Kua-toa driver was lashing together the boats to account for the loss of his companion. Vanko declared the driver's name to be "Trent" and declined the wager. The maintenance was completed in 45 minutes.

Small talk dominated the remainder of the watch largely focused around Jimjar's origins.

### Watch 2

Derendil and Vanareth

Derendil asks Vanereth about his homeland and asks if he could point out Kath on a map. However, Derendil was unable draw a map so Vanereth began drawing a map of Whale Tooth Island showing that he was originally from Diambole (rather than Kath).

### Watch 3

Dessa and Eldeth

Uneventful.

### Watch 4

Takintotle and Buppido

Buppido tells Takintotle that Derro poison their bread in pockets that they eat around. Water boiling happens during the watch and Buppido helps Takintotle make breakfast in the morning.

## Morning

4 lbs of food and 2 gallons of water net used for breakfast.

Buppido got his revenge on Vanareth with some Torchstalk which erupted in flame dealing 1 damage to Vanareth, Jun, and Vanko. Vanareth basically thought it was spicy.

6 Kua-toa rose from the water and invited the party to the wedding as exotic guests. The answer was required in short order as the wedding was immenent. Unsurprisingly, the party agreed to trust these random strangers and attend the joining of Bloopblap and Bloopbloop.

### The Wedding

The party was given Waterbreathing via ritual casting by the Kua-toa as the wedding was taking place in an underwater township. An open question was raised around Stool and his inability to swim leading to a debate on who might need to remain behind.

Sarith and Hemeth remained behind with Stool.

#### Shark Attack

While swimming towards the wedding, the wedding party was attacked by sharks. Dessa, in her infinite wisdom, swam towards the sharks at the start of the encounter. Jun, having issues with the dark, got a handwaved Darkvision from one of the Kua-toa clerics.

The sharks, sensing the harmed party members, triggered their Blood Frenzy against the damaged members of the party. Dessa got bit twice rapidly panicking the Kua-toa who rushed to protect their "exotic" wedding guests netting one of the sharks.

Takintotle and Jimjar took potshots. Buppido poisoned one of the sharks rendering it unconcious. Jun no Suke successfully finished off the unencumbered shark with his spear and threw two quick punches at the netted shark. Psychic blades finish off the netted shark and the remainder is killed while asleep. 

#### At the Altar

Takintotle worked on the line to render the sharks. Vanko had a gigantic spectacle on the altar with the brazier in order to summon a new familiar and distract the attendants. Jimjar used the confusion to take a little bit from the gold pile.

When the ceremony started, a gigantic psychic aquatic creature (an Aboleth) came to facilitate the 'joining' which was revealed to not be a wedding. The process involved combining the two Kua-toa with a mucus membrane into a single being in a confusing mix of limbs and fish parts.

Takintotle was so traumatized by the event he broke down and just started weeping. Eldeth began hallucinating and Derendil simply lost his ability to react for a period.

In the after parts of the ceremony, Dessa caught a spleen thrown in the same manner as a bouquet which indicated that she would be the next to participate in a joining.
