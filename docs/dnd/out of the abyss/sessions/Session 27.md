# Session 27

Introducing Lorian

## Encounter



## Watch

### Watch 1

* Vaako
* Eldeth

We ran into 5 Dueregard

Day 1: We're excited to go into the Underdark and trade with people

Day 5: It's pretty hard down here. People keep attacking us and assume that we're thieves

Day 20: Dayna was taken in our sleep. We don't know what happened

Day 25: Fauna attacked us and we died

Day 30: Some local traders came by.

Day 40: I can't be a trader down here anymore. It's too dangerous.

Day 45: I don't like it, but we killed some travelers in their sleep. And for the first time, I didn't go to bed hungry.

Day 50: Some Drow surprised us in our sleep. I thought we were done for, but they offered us a deal. They would show us the way out if we attacked a sleeping party.

### Watch 2

* Jun No Suke
* Stool

### Watch 3

* Adira
* Lorian

### Watch 4

* Takintotle
* Derendil