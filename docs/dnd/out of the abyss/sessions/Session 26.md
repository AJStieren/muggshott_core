# Session 26

The party is continuing to advance towards Blingdenstone. Currently shooting for the Faerezzness, the party intends to forego the safety of Vaako's safe house (church of Vecna) despite food and water shortages.

There are 2 mines and a quarry close to the city.

## Jorlan is Here

Jorlan showed up to warn us of an impending ambush.

## Watch 4

Jimjar and Takintotle make breakfast. Jimjar bets Takintotle that he will be a perfect sous chef and does a remarkable job (19). Takintotle is flustered by this turn of events and does relatively poorly with breakfast (12). Subsequently, Takintotle claims Jimjar has not been the perfect sous chef given how he flustered Takintotle. Jimjar complained about semantics, but the bet amount was zero so it was irrelevant.

## Day 3

### Encounter

A line of Modron were encountered heading South. The party had no method to go around them as they approached so Adira cast an illusion to hide the party which allowed them to successfully hide from the Modron.

Adira, feeling proud of herself, leaned against an object not realizing it was part of the illusion and fell. She grabbed Vaako's bagpipes to steady herself which let out a sound, but was caught and held up by Derendil. The Modron took literal notes of the occurrence, but otherwise continued on their march.

### Foraging

16 lbs Trilimac
3 Fire Lichen

### Watch 1

* Vaako
* Jimjar

Vaako tells Jimjar that each safe house is different when Jimjar inquires about the location of valuables. Jimjar makes a note of that.

Perception: 9

When Vaako goes to sleep she has a nightmare of Modron booing at her while she is on stage with her bagpipes. They are described as hideous creatures with grotesque faces.

### Watch 2

* Jun no Suke
* Stool

Jun and Stool practice martial arts. Stool does well and begins to come to the conclusion that the biggest part of martial arts is looking cool while performing.

Percetion: 21

Jun has a nightmare of being lost. He comes across Knight Sir Mixalot in his dream and is a teller of truth who like large butts, but has the sudden realization that it might be his twin who is a teller of lies who despises large butts. For the rest of his dream, he is panicked trying to come up with a question for the knight.

### Watch 3

* Adira
* Derendil

Adira continues trying to 'teach' Derendil Undercommon, but is really trying to coax him into realizing 

Derendil has a sister, loving parents. His best friend was a wizard but he believes that the friend might have been responsible for changing him and putting him in the Underdark. Adira has never heard of the nation Derendil is from. He used to live in a castle by the sea.

Derendil says he would rather take the blinding sand of the desert than the darkness of the Underdark any day.

Perception: 6

In her trance, Adira has disturbing nightmarish visions. She sees the Modrons walking across a bridge and she cuts the bridge to see them all fall. She cuts the bridge to watch the Modron fall only to realize her goal was on the other side of the bridge. Going to the next bridge, she finds it burned only to realize she is holding the match. Throwing away the lit match, it ignites a bunch of exploding mushrooms which eliminates a third bridge.

Adira's (Warlock?) patron appears and lectures her about being able to exploit friends and not burning bridges.

### Watch 4

* Takintotle
* Eldeth

Takintotle's Nightmare: Takintotle wakes up in his bakery and begins getting ready for the day. He finds that the Modrons have opened up a business across the street and finds himself unable to understand what their business is as it only opens while he is asleep.