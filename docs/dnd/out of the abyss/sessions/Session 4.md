# Session 4

Takintotle cooks morning meal with Dessa's help (23 Cooking Utensils). Consumes 6 pounds of rations for first meal requiring a second meal in the evening.*

*Really I'm just splitting food to preserve in case more people abandon the party or die.

The party agrees to follow [Shuushar](../npcs/Shuushar.md), but needs to find a path he is familiar with in order to continue. A fast pace is chosen in order to make progress away from the pursuers for the first 2 days and a slower pace is elected on the third.

## LAVA!!

A lava flow erupted starting an encounter with twelve fire beetles. They weren't good fire beetles. [Shuushar](../npcs/Shuushar.md) got super burned and stranded in the lava flow.

By the beginning of round 2, the fire beetles were dealth with and [Shuushar](../npcs/Shuushar.md) had received heals from [Dessa](../party/Dessa.md) and [Vaako](../party/Vaako.md)

## Foraging and Watch Round 1

Foraging was successful and watches were uneventful. Takintotle and Derendil bonded.

### Foraging

[Vaako](../party/Vaako.md) and [Buppido](../npcs/Buppido.md) followed by Jimjar. Jimjar bets 5g that he can find more water.

[Jimjar](../npcs/Jimjar.md) finds 14 gallons of water worth of ice from an ice spire he pulled.

Vaako finds 7 gallons of water and 9 pounds of food

### Watch 1

[Vanereth](../party/Vanereth.md) and [Shuushar](../npcs/Shuushar.md)

### Watch 2

[Dessa](../party/Dessa.md) and [Sarith](../npcs/Sarith.md)

### Watch 3

[Jun no Suke](../party/Jun no Suke.md) and [Stool](../npcs/Stool.md)

### Watch 4

[Takintotle](../party/Takintotle.md) and [Derendil](../npcs/Derendil.md)

## Campfire Encounter

The party came across a campfire in the underdark. [Vaako](../party/Vaako.md), [Sarith](../npcs/Sarith.md), and [Vanereth](../party/Vanereth.md) were volunteered to investigate. The drow around the campfire wore garbs from Menzoberranzan.

[Sarith] was familiar with one of those around the fire. Unfortunately for him. He was informed that his family name has garnered a bad reputation and that it would not be in his interest to return to the city. Sucks for him.

They had been expelled from Menzoberranzan for opposing the matriarchy. 

[Takintotle](../party/Takintotle.md) and [Jimjar](../npcs/Jimjar.md) stealthed behind the negotiation party and stole from the group. [Takintotle](../party/Takintotle.md) was critically unsuccessful in his stealth, but was still able to rifle through the drows' belongings thanks to the distraction of [Vaako](../party/Vaako.md).

Jimjar's loot:

* Water Skin
* Water Skin
* Water Skin with rum
* 50 gold pieces
* Tear drop earing

Takintotle's loot:

* Nice robes
* 5 Loaves of spore bread
* Hunk of Cheese
* Underdark spice
* Potion of Climbing
* Bag of Holding

Takintotle omits the Potion of Climbing and insinuated the Bag of Holding is a normal bag in the tally to Jimjar. Lying to his face to get 5 gp.

Takintotle promptly makes the most amazing stuffed mushrooms out of the cheese, two loaves of bread, and some gathered mushrooms.

## Foraging and Watch Round 2

### Watch 1

[Vaako](../party/Vaako.md) and [Sarith](../npcs/Sarith.md)

A discussion about the matriarchy and the cult of [Vecna](https://en.wikipedia.org/wiki/Vecna). Also... why does a cult of "The Undying King" have proselytizers?

### Watch 2

[Vanereth](../party/Vanereth.md) and [Derendil](../npcs/Derendil.md)

A... violent debate on what is more fictional: Kath or Nelrindenvane

### Watch 3

[Jun no Suke](../party/Jun no Suke.md) and [Shuushar](../npcs/Shuushar.md)

Something something something aligning your chi.

### Watch 4

[Takintotle](../party/Takintotle.md), [Stool](../npcs/Stool.md), and [Eldeth](../npcs/Eldeth.md)

Discussion on the native pastries of Gruntlgrim. Churos and forge cakes

## Encountered Blurg

See [Blurg](../society of brilliance/Blurg.md)
