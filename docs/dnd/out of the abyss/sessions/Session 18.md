# Session 18

I got sad due to an IRL issue with Joel and missed part of the initial conversation.

## Gray Ghost Aftermath

While Takintotle made snacks, the remainder of the party went about investigating and looting the camp.

The party had wiped out a ritual on the platform intended to mess with the Stone speakers. In the center of the circle was a statue of one of the Stone Speakers with a second head in the process of growing. Given the nature of the madness and the names on the lists in a note from Droki found at the camp, it was concluded that this was the sources of the giants' madness.

A shrieker was found and swiftly dealt with over the course of the investigation.

## Zombieland

The party came across a Derro standing at the top of a ravine filled with zombies. Takintotle and Jimjar killed him stealthily and dumped his body into the ravine. While atop the ledge, an animated hand with a ring was spotted amongst the mushrooms. Adira put together that this was the hand belonging to the ghost which appeared after Buppido's death, something that needed to be returned to his family.

Bruce the Bat wrestled with the hand for a while and brought it to the party.

## Specimen Keeper


