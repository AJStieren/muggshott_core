# Session 3

Starting off in the mushroom forest. Technical issue regarding vision with Matt and Max.

Currently eating a full meal which doesn't count against foraged foods due to location within the forest.

Started off the morning with song [Robin Hood Da Disney](https://www.youtube.com/watch?v=HJog7PfkNRY)

Sarah talking about eating Derendil

Ambushed due to dicking around in the morning

Provided Silken garments to Larissa

Used 9 lbs of food (Max had another 10)

Vanaris (Matt) went foraging for water and came up with 15 gallons of water and 10 lbs of food.

## Ambush

Started with a volley of arrows.

Junsoke (Max) and Derendil went front line. Junsoke spent most of his ki points.

Larissa baned early but only one target failed. Killed one of the Drow

[Shoor](../enemies/Shoor.md) sucked with the wand and missed twice.

Cut bridge and run to escape.

### Enemies

[Ilvara](../enemies/Ilvara.md)
[Jorlan](../enemies/Jorlan.md)
[Shoor](../enemies/Shoor.md)

Quaggoth 1
Quaggoth 2 - dead

Drow Guard 1
Drow Guard 2
Drow Guard 3 - dead

Giant Spider (Summoned)
Giant Spider (Summoned)

## Watches

Takintotle takes watch 4/4

Matt + Larissa watches 1 and 2

Sarah takes watch 3

## NPC Assignments

## Matt

Shuushar
Serath

## Sarah

Buppido
Topsy

## Me

Jimjar
Stool

## Max

Derendil

## Larissa

Eldeth
Turvey
