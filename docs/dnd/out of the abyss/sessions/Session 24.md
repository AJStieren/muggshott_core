# In Person

This session was in person. It's the first time we've actually met in person. 

## Solidarity Menu

The new solidarity game is guessing what is for dinner. This week was pizza (decent place).

All 4 children were present at dinner as well as associated spouses which explains the pizza. Pizzas were mushroom, cheese, and pepperoni. A kid friendly menu is likely to be a theme.

## The Session

The party entered the session immediately after an encounter with a "friendly" beholder missing a few eyes. After lying their way through and abusing Adira's genuine background connections, the party was on their way to a different location. Takintotle of course had to do some cooking for the group in order to curry favor.


