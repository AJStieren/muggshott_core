# Session 23

## The Party Departs

The party elected to take Stool (via Takintotle's wish). As a "reward", Stool was granted the Sovereign Spore ability.

> When under the effect of Rapport Spores, +2 on Madness checks.

The escort agreed to 3 days

## Food stuffs

Barrelstalk from the group pouch was liquidated into food along with the remaining rations for food. In order to facilitate the process, the total amount was reduced to raw lbs or servings for a medium creature giving the equivalent of the following:

157 rations
52 gallons of water

This liquidated everything except seasonings and the following from group:

* 15 Bluecap
* 10 Ripplebark
* 5 Trilimac

## Duereggar Ruins

Found amongst the ruins is a 1 oz block of some unknown material.

A bag of 47 humanoid teeth are found. 1 of them is rotten.


