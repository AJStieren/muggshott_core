# Session 7

Gotta repair boat 2 from the previous session. Shuushar is officially gone. 

There was a mismatch in the tracked food and water in which I had 57 food an Blaise had 36. After clearing 9 missing food from the first day rooted in a ruling allowing the use of [Takintotle](../party/Takintotle.md)'s feat and background of Chef and Cook respectively. This was resolved by ruling the additional 12 lbs of food were ruined in the rapids rather than a quarter of what would've been the food in question for his tally.

## Watch Round 1

### Watch 1

[Vaako](../party/Vaako.md) and [Eldeth](../npcs/Eldeth.md)

Eldeth refused an invitation from Vaako to join the cult of Veccna.

[Buppido](../npcs/Buppido.md) woke up at the end of the watch from a nightmare and desired conversation. Eldeth scurried to bed early in order to avoid further conversation with Vaako.

### Watch 2

[Vanereth](../party/Vanereth.md) was woken by Vaako.

Vanereth had a cordial conversation with Buppido before ushering the Deep Gnome back to sleep. Once asleep, Vanereth chose to snoop through Buppido's belongings but clumsily got caught. Vanereth claimed to have been checking to see if he had been hoarding food as he screamed bloody murder to wake up the camp.

Takintotle, Jimjar, and Jun did not wake. Jimjar was woken by Vaako to make a bet on who would win in the apparently brewing fight between Vanereth and Buppido. Eventually the situation was defused and the remainder of the watch happened largely without incident.

### Watch 3

[Dessa](../party/Dessa.md) and [Jun no Suke](../party/Jun no Suke.md)

Once awoken, Vanereth whisper to Jun "If I die tonight, it was probably Buppido." Jun kicked dirt into Dessa's eyes. Dessa asked Jun to tell his story which... amounted him to achieving enlightenment from meeting Bob Levinstein who taught him kung fu and aligned his Chi.

### Watch 4

[Takintotle](../party/Takintotle.md) and [Derendil](../npcs/Derendil.md)

Boiled 2 gallons of water.

## Morning

Ate 4 lbs of food for half the day and 4 gallons of water.

### Traveling

The party was ambushed by Merrow who ate one of the Kua-toa.

