# At A Glance

Quick reference for immediate uses.

## Double-Breadth Marching Order

![Two people wide](../../img/dnd/out of the abyss/Double_Breadth_Marching_Order.png "Marching Order")

## Foraging

The food currently foraged by the group.

### Total Food (Current)

8 lbs of food [mushrooms](Mushrooms.md)
11 lbs of Barrelstalk
16 lbs of Zurhkwood
14 lbs Trilimac


52 Gallons of Water

17 Rations

#### Breakdown

27 lbs of [Zurhkwood](./Mushrooms.md#Zurhkwood)
4 Gallons of [Waterorb](./Mushrooms#Waterorb)

##### Session 3 Gathering

10 lbs of food
15 Gallons of Water

6 lbs of mushrooms into flour

##### Session 4 Gathering

9 lbs of food
21 Gallons of Water
5 loaves of spore bread
1 Hunk of cheese

6 lbs of mushrooms into flour

##### Session 5 Corrections???

Boiled gallon of water

##### Session 6 Aftermath

10 lbs of Mushroom flour ruined
2 loaves of spore bread ruined

Gathered 3 gallons of water from stream

##### Session 7

Boiled 2 gallons of water during watch

##### Session 8

Boiled 2 gallons of water during watch

Consumed 2 lbs of flour, a loaf of spore bread, and a ration for breakfast. Also 4 gallons of water.

##### Session 9

Gathered 12

##### Session 10

Gathered 50 lbs of spider meat

##### Session 19

Circlet of Blasting (Takintotle)
Stone of Luck (Jun)
Gloves of Thieving (Vaako)
???? (Adira)
250 gp of supplies per person
500 gp (125 per player)

##### Session 24

Takintotle went crazy and cooked most of the food. Like almost 3/4's of it. And virtaully all of the water.

##### Session 25

_Foraging_
Barrelstalk (Adira):
- 17 gallons of water
- 15 lbs of food

Fire Lichen (Vaako):
- 10 uses
- Dueregar Fire Liquor

Zurhkwood (Jun no Suke);
- 28 lbs

Nilhogg's Nose (Takintotle):
- 5 doses
- Capsule of Tinmask (confusion smoke bomb)

Ate 12 lbs of food and drank 6 gallons of water

##### Session 26

_Foraging_

Takintotle: 16 lbs Trilimac

Jun no Suke: 3 Fire Lichen

_Consumed_

12 lbs of food
12 gallons of water

## Group Items

3 potions (2 identical)
scroll
strand of pearls

2 scrolls of raise dead

83 good rations
9 water rations

31 gallons
44 food from barrelstalk

157 food total
52 gallons of water total

15 Bluecap
10 Ripplebark
5 Trilimac

Here is what I have thus far from base rules:
Cloak of Many Fashions - 50gp
20 lbs of flour - 40 cp
4 lbs of salt - 20 cp
1 lbs of ginger - 1 gp
1 lbs of pepper - 2 gp
1 lbs of cinnamon - 2 gp
1 lbs of cloves - 3 gp
5 loaves of bread - 10 cp
5 cheese hunks - 5 sp
2 bottles of wine - 20 gp
    - 2 glasses
1 Gallon of ale - 2 sp
12 waterskins - 24 sp
Orb of Direction - 50 gp

## Other Loot

Silken garments
Silver Headress
Drawstring bag
- 2 potions healing
Purse
- 20 g
- 20 s
Small moonstone
Component pouch

Underdark spice
Potion of Climbing
8 Spears
7 Nets
8 Shields (they be gross)
9 Kua-toa garments

Javelin
Journal (in goblin)
2 Scimitar (covered in poison)
Alchemist Fire x3
4 Scimitar

