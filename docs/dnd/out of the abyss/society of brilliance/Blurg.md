# Blurg

*Race:* Orog | *Gender:* Male

## Encounter

Blurg was encountered investigating the "Faerzress" in a cave. He was asked for directions on getting to Sloobludop and cut the remaining journey down to a day before crossing a river.

From Sloobludop he suggests a raft to Gracklstugh with currents and heading north to Neverlake Grove.

Apparently the Faerzress is capable of blocking spells. It cannot be teleported into, out of, or within. The attempt will cause pain. It's also capable of hiding one from magic.

Blurg is a member of the Society of Brilliance. The society solves the problems of the Underdark.

Blurg likes "churros".

Blurg suggests going to Blingdestone to leave the underdark. It is 16 days from Neverlake Grove mostly north by northwest. A guide is recommended.

## Society of Brilliance Members

Grazilax - Mindflayer
Sloopidoop - Kuatoa
Skriss - Troglodyte
Y - Derro Savant
