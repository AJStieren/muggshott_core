# Out of the Abyss

Campaign Module: Out of the Abyss <br>
DM: Blaise

## Sessions

* [Session 1](sessions/Session%201.md)
* [Session 2](sessions/Session%202.md)
* [Session 3](sessions/Session%203.md)
* [Session 4](sessions/Session%204.md)
* [Session 5](sessions/Session%205.md)
* [Session 6](sessions/Session%206.md)
* [Session 7](sessions/Session%207.md)
* [Session 8](sessions/Session%208.md)

## Party

* [Takintotle](party/Takintotle.md) - Me
* [Matt](party/Vanereth.md)
* [Larissa](party/Dessa.md)
* [Sarah](party/Vaako.md)
* [Max](party/Jun%20no%20Suke.md)

## NPCs

### Party NPCs

* [Buppido](npcs/Buppido.md)
* [Derendil](npcs/Derendil.md)
* [Eldeth](npcs/Eldeth.md)
* [Jimjar](npcs/Jimjar.md)
* [Sarith](npcs/Sarith.md)
* [Stool](npcs/Stool.md)


### Former Party NPCs

* ~~[Ront (deceased)](npcs/Ront.md)~~
* [Shuushar](npcs/Shuushar.md)
* [Topsy & Turvey](npcs/Topsy & Turvey.md)

### Society of Brilliance

* [Blurg](society of brilliance/Blurg.md)
* [Grazilax](society of brilliance/Grazilax.md)
* [Skriss](society of brilliance/Skriss.md)
* [Sloopidoop](society of brilliance/Sloopidoop.md)
* [Y](society of brilliance/Y.md)

## Enemies

* [Ilvara](enemies/Ilvara.md)
* [Jorlan](enemies/Jorlan.md)
* [Shoor](enemies/Shoor.md)
* [Demogorgon](enemies/Demogorgon.md)
