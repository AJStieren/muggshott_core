## Mushrooms

### Waterorb

A waterorb is a bulbous fungus that grows in shallow water. A mature waterorb can be squeezed like a sponge, yielding a gallon of drinkable water and a pound of edible (if chewy and somewhat tasteless) food.

### Zurhkwood

Zurkhwood is a massive mushroom that can reach a height of thirty to forty feet. Its large grain-like spores are edible and nutritionally equivalent to pounds of food, but zurkhwood is more important for its hard and woody stalks. Zurkhwood is one of the few sources of timber in the Underdark, used to make furniture, containers, bridges, and rafts, among other things. Skilled crafters can use stains, sanding, and polishing to bring out different patterns in zurkhwood.

### Fire Lichen

Pale orange-white in color, fire lichen thrives on warmth, so it grows in regions of geothermal heat. Fire lichen can be ground and fermented into a hot, spicy paste, which is spread on sporebread or added to soups or stews to flavor them. Duergar also ferment fire lichen into a fiercely hot liquor.

### Torchstalk

A one- to two-foot-tall mushroom with a flammable cap, a single torchstalk burns for 24 hours once lit. 1 in 6 chance it will explode when lit.

### Barrelstalk

A barrelstalk is a large, cask-shaped fungus that can be tapped and drained of the fresh water stored within it. A single barrelstalk contains 1d4+4
 gallons of water and yields  pounds of food.

### Bluestalk

Dubbed the “grain of the Underdark,” a bluecap is inedible, but its spores can be ground to make a nutritious, bland flour. Bread made from bluecap flour is known as sporebread or bluebread. One loaf is equivalent to 1 pound of food.

The caps of bluecap mushrooms could be prepared by those with the correct herbalism knowledge to make a stew that would neutralize poison

### Ripplebark

Ripplebark is a shelf-like fungus that resembles a mass of rotting flesh. It is surprisingly edible. Though it can be eaten raw, it tastes better roasted. A single sheet of ripplebark yields pounds of food.

### Trilimac

A trillimac is a mushroom that grows to a height of four to five feet, and has a broad gray-green cap and a light gray stalk. The cap’s leathery surface can be cut and cleaned for use in making maps, hats, and scrolls (its surface takes on dyes and inks well). The stalk can be cleaned, soaked in water for an hour, then dried to make a palatable food akin to bread. Each trillimac stalk provides pounds of food.

### Bigwigs 

A bigwig is a four-inch tall mushroom with a thin steam and a wide purple cap. A creature that eats one… grows in size as though under the enlarge effect on an enlarge/reduce spell. The effect lasts 1 hour." Bigwigs are found in the Whorlstone Tunnels of Gracklstugh, and a faerzress-suffused, which grants them their unique properties.

 
### Nightlight

A nightlight is a tall and tube-shaped bioluminescent mushroom that grows to a height of feet and emits bright light in a 15-foot radius and dim light for an additional 15 feet.

### Nilhogg’s Nose

A Nilhogg’s nose is a small mushroom that grants any creature that eats it advantage on Wisdom (Perception) checks based on smell for a number of hours. 

### Ormu

A bioluminescent green moss that grows in warm and damp areas, ormu is particularly common near steam tunnels and vents. It sheds dim light in a 5-foot radius, and can be harvested, dried, and made into a phosphorescent powder or pigment.

### Pigmiwarts 

A pygmywort is a mushroom with a one-inch long stem and a stubby boysanberry blue cap with white dots. A creature that eats one… shrinks in size as though under the enlarge effect on an enlarge/reduce spell. The effect lasts 1 hour." Pygmyworts are found in the Whorlstone Tunnels of Gracklstugh, and a faerzress-suffused, which grants them their unique properties

### Timmask

a timmask is a two-foot-tall toadstool with orange and red stripes across its beige cap.

### Tongue of Madness

Tongue of madness is an edible fungus that looks somewhat like a large human tongue. 

### Torchstalk

A one- to two-foot-tall mushroom with a flammable cap, a single torchstalk burns for 24 hours once lit.
