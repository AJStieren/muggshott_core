# Creature/Character

Name: <br>
AC: <br>
Speed: <br>
Proficiency Bonus: <br>
Initiative: <br>

HP: <br>

Str | Dex | Con | Int | Wis | Cha
--- | --- | --- | --- | --- | ---
10  | 10  | 10  | 10  | 10  | 10

## Attack

Attack: <br>
Bonus: <br>
Damage: <br>

## Skills

## Abilities

## Items

## Notes