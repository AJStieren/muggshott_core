# Template

Campaign: {Module or campaign title} <br>
DM:

## Party

* [Sample](party/sample.md)

## Sessions

* [Session Sample](sessions/sample.md)

## NPCs

* [Sample](npcs/sample.md)
* [Sample+](npcs/sample_plus.md)

## Enemies

* [Sample](enemies/sample.md)

## Factions

* [Sample](factions/sample.md)

## Locations

* [Sample](locations/sample.md)

## Quests

* [Sample](quests/sample.md)

