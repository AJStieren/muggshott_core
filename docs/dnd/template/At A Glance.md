# At A Glance

Quick reference for immediate uses.

## Party Purse

This is a location for tracking the party purse.

### Currency

PP   | GP   | EP   | SP   | CP
---- | ---- | ---- | ---- | ----
0    | 0    | 0    | 0    | 0

### Group Items