# Dungeons and Dragons

Every other week with a number of exceptions throughout the year, I attend a tabletop gaming session which largely uses the 5th Edition ruleset of Dungeons and Dragons. A sample for campaigns can be found [here](template/README.md) while other campaigns may be found below.

## Campaigns

* [Out of the Abyss](out of the abyss/README.md)
* [Gethenhol](gethenhol/README.md)
* [Template](template/README.md)

![Rollout](../img/dnd/out%20of%20the%20abyss/BOULDER.png "Keep Rolling On")