# Petrochem

```mermaid
graph LR

Charcoal --> Carbon
Carbon -->|steam| Monoxide
Monoxide -->|steam| Dioxide
Monoxide -->|steam| Hydrogen
Dioxide -->|green catalyst| Methanol
Water --> Oxygen
Water --> Hydrogen
Air --> Oxygen
Air --> Nitrogen
Nitrogen -->|red catalyst| Ammonia
Hydrogen -->|red catalyst| Ammonia
Ammonia --> Urea
Dioxide --> Urea
Methanol -->|steam| Propene
Methanol -->|steam| Residual_Gas
Methanol -->|green catalyst| Formaldehyde
Urea -->|blue catalyst| Melamine
Propene --> Plastic
Melamine --> Resin
Formaldehyde --> Resin
```