# Self-Service

## Overview

For Cloud Feeds to be successful, the service should not require app deployments--the app should be fully configurable through an API. Currently adding new feeds and schemas is not API driven and requires a manually intensive process. 

## Context

Changes can have month long turn arounds due to combined features and limit release windows on the service. The reasons for the turnaround time are:

* It's a very manual process which involves 3 repos and setting up
* OPS Deployment dependency and access restrictions are barriers to common automated deployment strategies

## Goals

* Add new feed to a WADL and run the standard-usage-schema compilation process which entails generating WADLs, XSDs, and XSLTs
* Deploy new WADLS & associated files to Repose
* Update atomhopper.xml configuration files (and deploy to atomhopper)
* Update service catalog XML files (and deploy)

Encapsultating this within an API will entail a major rewrite of all the above processes.

## Adding/Modifying a feed

### Existing Solution

The onboarding process for adding a new feed currently involves submitting a PR against the schema repo, that PR being reviewed by a Feeds dev, manually syncing any changes to the generated XSLTs to the cloudfeeds-nabu repo for archiving, manually adding regression test cases, manually creating release rpm artifacts, and deploying to nodes using an ansible script.

![Schema Insert Sequence Diagram](../img/plantuml/schema-insert-current.png)

### Proposed Solution

In order to simplify the process for simple schema adjustments it is being proposed to add a validating controller into the API in order to allow a publisher to make limited schema adjustments bypassing the majority of this process. While the changes permitted would be restricted primarily to superset of any existing schema or entire versioning/feed additions, the allowance of API control would eliminate the manual wadl compilation steps.

### Testing

## Update Service Catalog