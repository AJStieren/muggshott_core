# Self Service Scratch

* Add Spring Boot Object Definition to **Schema**
* Spring Boot Object to XSD (Saxon EE)
* 

## Next Steps

Catalog Upgrade Spring to Spring Boot
Catalog Logging Enhancement
Schema Spring Boot Beans

## Components

### REST Controller

Options
* ~~Schema Repo~~
* Catalog
* New Repo

#### Catalog Option

Currently on Spring. Target is Spring Boot.

* Upgrade Catalog to Spring Boot
* Add Controller For Self Service
