# Core Docs

This is the base of the repository documents for this utility repo using the [mkdocs](http://mkdocs.org) specification.

## Layout

* [AFK Arena](afk/README.md)
* [Antimatter Dimensions](antimatter/README.md)
* Cloud Feeds
* [Dungeons & Dragons](dnd/README.md)
* [Homelab](homelab/README.md)
* [Keebs](keebs/README.md)
* [Minecraft](minecraft/README.md)
* [Personal](personal/README.md)
* [Pokemon](pokemon/README.md)
* [Profession](profession/README.md)
