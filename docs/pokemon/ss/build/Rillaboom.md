# Rillaboom

Grass<br>
Base Stats      | ###    
--------------- | --- 
HP              | 100 
Attack          | 125 
Defense         | 90  
Special Attack  | 60  
Special Defense | 70  
Speed           | 85  

Overgrow:
Grassy Surge:

## Choice Band

Nature: Adamant | +Atk, -SpA<br>
Ability: Overgrow/Grassy Surge<br>
Item: Choice Band<br>
EVs: 4 HP | 252 Atk | 252 Spe<br>
1. Wood Hammer / Drum Beating
2. U-Turn
3. Grassy Glide
4. Knock Off / High Horsepower

## Bulky Surfer

Nature: Adamant | + Atk, -SpA<br>
Ability: Grassy Surge<br>
Item: Assault Vest<br>
EVs: 140 HP | 252 Atk | 4 Def | 84 SpD | 28 Spe<br>
1. Grassy Glide / Drum Beating
2. U-Turn
3. Hammer Arm / Superpower
4. Knock Off / High Horsepower

## Sub-seeder

Nature:<br>
Ability: Grassy Surge<br>
Item: Leftovers<br>
EVs:<br>
1. Drum Beating
2. Knock Off 
3. Leech Seed
4. Protect / Substitute

## Terrain Seed

Nature: Adamant / Jolly | +Atk, -SpA / +Spe, -SpA<br>
Ability: Grassy Surge<br>
Item: Grassy Seed<br>
EVs: 252 Atk | 4 Def | 252 Spe<br>
1. Swords Dance
2. Grassy Glide
3. Acrobatics
4. High Horsepower/Knock Off
