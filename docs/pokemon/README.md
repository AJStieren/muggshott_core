# muggshott_core: Pokemon

Pokemon comes in a number of generations. The current generation will always be the most prevalent at the time.

Current generation: Sword and Shield (SS)

## General Favorites

I have a natural number of favorites for whatever reason which may be tracked.

## Pokemon Builds

Builds are competitive or at least complete sets with potential movesets. See below for a generic template

### Pokemon Species nXXX

Type 1 / Type 2

<table>
    <tr>
        <td colspan="2">Base Stats</td>
    </tr>
    <tr>
        <td>HP</td>
        <td>100</td>
    </tr>
    <tr>
        <td>Attack</td>
        <td>75</td>
    </tr>
    <tr>
        <td>Defense</td>
        <td>75</td>
    </tr>
    <tr>
        <td>Special Attack</td>
        <td>75</td>
    </tr>
    <tr>
        <td>Special Defense</td>
        <td>75</td>
    </tr>
    <tr>
        <td>Speed</td>
        <td>75</td>
    </tr>
</table>

Ability: A description of the ability
Hidden Ability: A description of the hidden ability

#### Pokemon Build

Nature: Nature Name (+ Stat, -Stat)<br>
EVs: X HP | X Atk | X Def | X SpA | X SpD | X Spe<br>
Move 1: Move/Move/Move<br>
Move 2: Move<br>
Move 3: Move/Move<br>
Move 4: Move/Move

Description of build intent and play method.
