# Keebs

Keyboards, specifically mechanical keyboards, are back to being an obsession of mine. I'm fascinated on the permutations of boards that can be made as well as the specific changes in feel given certain activities. This entire hobby is needlessly bougie if not ouright wasteful. Having said that, it's a fascinating world and one of the few hobbies I feel like I want to get hands on with. It's also a good introduction into the basics of soldering and working with circuit components.

## The Boards

* [Corsair K95 Platinum](boards/k95/README.md)
* [Drop Ctrl](boards/ctrl/README.md)
* [Drop Ctrl Hi-Profile](boards/ctrl-hi/README.md)
* [Keychron K6](boards/k6/README.md)
* [Mountain Everest](boards/everest/README.md)

## The Parts

The elements of a keyboard are fairly straight forward. While not all boards are created equal, the extraneous layer of elements can basically be classified as `features` meaning a component that is not ubiquitous for a keeb. Here is a list of the elements:

* Case
* Features
* Interface
* Keycaps
* PCB or Plastic Circuit Board
* Plate
* Stabilizers
* [Switch](switches/index.md)
