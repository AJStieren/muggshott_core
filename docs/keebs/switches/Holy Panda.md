# Holy Panda

Make: Frankenswitch (Invyr + Drop) <br>
Model: Cherry MX Base <br>
Type: Tactile <br>
Spring: 67 grams <br>
Housing Material: POM <br>
Switch Material: POM <br>
Source: Made from Invyr Panda origin run and Halo True second run <br>
Price: Hand made (estimated $5 price point due to original run Panda base)
