# Switches

The switch is mechanical component to the keyboard which actuates to perform the operations. Switches can be wired individually, soldered into place on the PCB, or inserted into hotswap sockets depending on the board design. Most consumer boards come with soldered switches which made modification difficult to impossible given my current level of proficiency.

These will be documentations for acquired or desired switches: 

* [Halo Clear](./Halo Clear.md)
* [Halo True](./Halo True.md)
* [Invyr Panda](./Invyr Panda.md)
* [Holy Panda](./Holy Panda.md)
* [Unholy Panda](./Unholy Panda.md)
* [Glorious Panda](./Glorious Panda.md)
* [Gateron Brown](./Gateron Brown.md)
* [Gateron Yellow](./Gateron Yellow.md)
* [Cherry Brown](./Cherry Brown.md)
* [Otemu Brown](./Otemu Brown.md)
* [Kaihl Box Royal](./Kaihl Box Royal.md)
* [Polia](./Polia.md)
* Everglide Lime

## Template

Switch entries should follow a similar format as below

```
# Switch

Make: Kaihl, Cherry, Gateron, etc. <br>
Model: Cherry MX, Kaihl, Kaihl Box <br>
Type: Linear, Tactile, Clicky, or Other <br>
Spring: grams <br>
Housing Material: POM <br>
Switch Material: POM <br>
Source: Place(s) to buy the switch <br>
Price: Price per switch as a benchmark on its stock value (subject to change or revision) <br>
```

## Modifications

Several Modifications can be made to a switch to increase the smoothness or stability of it. Without even getting into the weird world of the Frankenswitch (see [Holy Panda](./Holy Panda.md)), these elements can be improved in a number of ways on their stock nature. Lubrication, an exchange of springs, or adding a film can all have minor effects on the spring and different methods apply to the different types of switches.

### Lubrication

Lubrication can be applied to the switch housing, stem, and spring. The kind and amount of lube varies between the components as well as differing strategies within the realm of what type of switch. Even differentiating between tactile switches can lead to complicated evaluations based on where the bump rests, though general rule of thumb is to not add any lubrication to the legs of the stem.

### Films

A film can be applied between the switch housings encompassing the stem. This film can vary in width, but is generally fractions of a millimeter in thickness. Materials vary from polycarbonates and foam to hard plastics with adhesive depending on the seller. Primary sources would be KDBFans and Deskey.

### Spring
Springs can be replaced at varying weights. Most springs are steel at stock, but a trend has developed for gold-plated springs to be used. Allegedly ping might be slightly reduced, however, I find that this is more of a matter of proper lubrication an appropriate switch stability. I should research that.

### Pin Clipping

A number of switches are shipped with a full 5 pin mounting housing on the bottom. These switches are intended for PCB mounts in which the switch is soldered into place directly on the PCB instead of being clipped through a plate mount. PCBs can be equipped with hotswap sockets, but most of those shipped with hotswap capabilites are not 5 pin compatible. In these cases, the additional 2 plastic pins can be clipped to fit into a 3 pin mount.

### O-Rings

Rings can be applied between the switch stem and the keycap. This raises the profile of the keys and results in a softer bottom out while dampening the sound during keypress.

## Terms

### Spring Ping

Spring ping refers to the reverberations from when a key is pressed and vibrates.

#### Everest Offenders

Currently getting some pinging sounds from the following:

* t
* y
* p
* 0
* 9
* 7
* up arrow
* right arrow

### Stability

The stability of a switch refers to the amount of vibration which occurs within the housing and stem during a key press. A more stable switch will have less rattling and produce a very short sound. Low stability can manifest as scratchiness, a hollowness in the keypress, or a "dirty" sound on the upstroke. 

