# Mountain Everest

## Board Summary

**Distributor**: [Mountain](https://mountain.gg) <br>
**Layout**: TKL with detachable numpad <br>
**Switches**: [Holy Panda](../../switches/Holy Panda.md) <br>
**Keycaps**: [Valiant](https://drop.com/buy/drop-skylight-series-keycap-set) <br>
**Stabilizers**: Factory

## Ongoing issues

### Escape

Sound disparity???

### 2

Sound disparity???

### Backspace

Rattlin on left stabilizer

### Spacebar

Rattling on both sides of stabilizer

### Left Shift

Rattling on left stabilizer

### Right Shift

Rattling across stabilizer

### Enter

Rattling on left stabilizer

### Numpad Enter

Rattling on bottom stabilizer

## Closed Issues

### typ097

Spring ping
