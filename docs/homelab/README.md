# muggshott_core: Homelab

The Gates homelab is in its infancy. The lab is based out of a Dell Poweredge r710 running Proxmox with a set of VMs.

## System

Proxmox<br>
├── pfSense<br>
└── MineOS<br>
    ├── mineos-001<br>
    └── mineos-002<br>

Kubernetes/Rancher/Docker<br>
├── Git Deployment<br>
│   ├── Git Tea/Lab<br>
│   └── DB<br>
├── Pod Management<br>
│   ├── Rancher/Portainer<br>
│   └── DB<br>
├── PlantUML<br>
└── Minikube<br>
